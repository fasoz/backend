import express from 'express';
import rbac, { Permission } from '../utils/rbac';
import jwt from 'jsonwebtoken';
import { AccessTokenPayload } from '../types';
import config from 'config';

const jwtSecret: string = config.get('jwt.secret');

/**
 * Stores permissions of the user making the request in the request object for later use.
 * @param request
 * @param _response
 * @param next
 */
export const permissionsExtractor = (
  request: express.Request,
  _response: express.Response,
  next: express.NextFunction,
): void => {
  if (!request.accessToken) {
    request.userPermissions = [];
    return next();
  }

  request.userPermissions = request.accessToken.roles
    .map((role) => rbac.getPermissionsForRole(role))
    .reduce((accumulator, permissions) => accumulator.concat(permissions), []);
  next();
};
/**
 * Extracts the payload from the access token and verifies its integrity.
 * @param request
 * @param _response
 * @param next
 */
export const accessTokenExtrator = (
  request: express.Request,
  _response: express.Response,
  next: express.NextFunction,
): void => {
  request.accessToken = null;

  const authorization = request.get('authorization');
  if (!authorization || !authorization.toLowerCase().startsWith('bearer ')) {
    next();
    return;
  }

  const token = authorization.substring(7);
  jwt.verify(token, jwtSecret);
  request.accessToken = jwt.decode(token) as AccessTokenPayload;
  next();
};
/**
 * Express middleware that denies requests from users that lack the necessary permission.
 * @param requiredPermission
 */
export const requiresPermission = (requiredPermission: Permission) => {
  return (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction,
  ): void => {
    if (!request.accessToken) {
      response.status(401).json({ error: 'access token missing' });
      return;
    }

    const userRoles = request.accessToken.roles;
    if (rbac.hasPermission(userRoles, requiredPermission)) {
      next();
      return;
    }

    response
      .status(403)
      .json({ error: 'Request denied. You lack the necessary permission.' });
  };
};
