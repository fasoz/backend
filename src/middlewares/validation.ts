/**
 * Validates the request body against the given schema. Returns 400 Bad Request if validation fails.
 * It also tries to cast incorrect types into what's expected and strips unexpected fields in the request body.
 * @param validationSchema - Yup schema that should be used for verification
 */
import * as yup from 'yup';
import express from 'express';
import logger from '../utils/logger';

/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
export const requestValidation = (validationSchema: yup.ObjectSchema<any>) => {
  return (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction,
  ): void => {
    if (
      !request.headers['content-type'] ||
      !request.headers['content-type'].match(/application\/json/g)
    ) {
      response
        .status(400)
        .json({ error: 'content-type must be application/json' });
      return;
    }
    validationSchema
      .validate(request.body)
      .then(() => {
        request.body = validationSchema.noUnknown().cast(request.body); // remove keys that aren't defined by the schema
        next();
      })
      .catch((error) => {
        if (error instanceof yup.ValidationError) {
          response.status(400).json({ error: error.message });
        } else {
          response.status(500).json({ error: 'unknown error' });
          logger.error(error);
        }
      });
  };
};
