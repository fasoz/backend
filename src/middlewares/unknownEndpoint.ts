import express from 'express';

/**
 * 404 response for unknown endpoints.
 */
export const unknownEndpoint = (
  _request: express.Request,
  response: express.Response,
): void => {
  response.status(404).send({ error: 'unknown endpoint' });
};
