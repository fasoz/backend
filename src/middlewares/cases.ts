import express from 'express';
import { Permission } from '../utils/rbac';
import Case from '../models/case';

/**
 * returns a function that checks if the user has the permissions to access this particular case.
 * The user ID is extracted from the access token. The Case ID is inferred from the URL Parameter.
 */
export const checkCaseReadPermissions =
  () =>
  async (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction,
  ): Promise<void> => {
    const caseId = request.params.id;
    const thisCase = await Case.findById(caseId);

    if (!thisCase) {
      response.status(404).json({ error: 'Case not found.' });
      return;
    }

    if (thisCase.published) {
      next();
      return;
    }

    if (
      request.accessToken &&
      request.userPermissions.includes(Permission.CaseReadAnyUnpublished)
    ) {
      next();
      return;
    }

    if (
      request.accessToken &&
      thisCase.owners.includes(request.accessToken.id) &&
      request.userPermissions.includes(Permission.CaseReadOwnUnpublished)
    ) {
      next();
      return;
    }

    response
      .status(403)
      .json({ error: 'Request denied. You lack the necessary permissions.' });
  };

/**
 * Returns a function that checks if the user has the permissions to update this particular case.
 * The user ID is extracted from the access token. The Case ID is inferred from the URL Parameter.
 */
export const checkCaseUpdatePermissions =
  () =>
  async (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction,
  ): Promise<void> => {
    if (!request.accessToken) {
      response.status(401).json({ error: 'access token missing' });
      return;
    }

    if (request.userPermissions.includes(Permission.CaseUpdateAny)) {
      next();
      return;
    }

    const caseId = request.params.id;
    const thisCase = await Case.findById(caseId);
    if (
      thisCase &&
      thisCase.owners.includes(request.accessToken.id) &&
      request.userPermissions.includes(Permission.CaseUpdateOwn)
    ) {
      next();
      return;
    }

    response
      .status(403)
      .json({ error: 'Request denied. You lack the necessary permissions.' });
  };

/**
 * Returns a function that checks if the user has the permissions to access file this particular case.
 * The user ID is extracted from the access token. The Case ID is inferred from the URL Parameter.
 */
export const checkFileAccessRoleRequirements =
  () =>
  async (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction,
  ): Promise<void> => {
    const caseId = request.params.id;
    const thisCase = await Case.findById(caseId);

    if (!thisCase) {
      response.status(404).json({ error: 'Case ID not found' });
      return;
    }

    const matchingRoles = thisCase.fileAccessAllowedRoles.filter((role) =>
      request.accessToken?.roles.includes(role),
    );
    if (matchingRoles.length < 1) {
      response.status(403).json({
        error: `Request denied. You need to have one of the following roles: ${thisCase.fileAccessAllowedRoles.join()}.`,
      });
      return;
    }

    next();
  };
