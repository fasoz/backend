import express from 'express';
import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';
import logger from '../utils/logger';
import { errors as elasticsearchErrors } from '@elastic/elasticsearch';

/**
 * Hides common exceptions from the client and sends nicely formatted json instead.
 */
export const errorHandler = (
  error: Error,
  _request: express.Request,
  response: express.Response,
  next: express.NextFunction,
): void => {
  if (error instanceof mongoose.Error.CastError && error.kind === 'ObjectId') {
    response.status(404).send({ error: 'invalid id' });
    return;
  } else if (error instanceof mongoose.Error.ValidationError) {
    response.status(400).json({ error: 'Validation Error' });
    return;
  } else if (error instanceof jwt.TokenExpiredError) {
    response.status(401).json({ error: 'token expired' });
    return;
  } else if (error instanceof jwt.JsonWebTokenError) {
    response.status(401).json({ error: 'token missing or invalid' });
    return;
  } else if (
    error.name === 'MongoServerError' &&
    error.message.startsWith('E11000')
  ) {
    response.status(400).json({ error: 'duplicate key error' });
    return;
  } else if (error instanceof mongoose.Error.MongooseServerSelectionError) {
    response.status(503).json({ error: 'document database unavailable' });
    logger.error('Document database error: ', error);
    return;
  } else if (
    error instanceof elasticsearchErrors.ConnectionError ||
    error instanceof elasticsearchErrors.TimeoutError
  ) {
    response.status(503).json({ error: 'search engine unavailable' });
    logger.error('Elasticsearch error: ', error);
    return;
  }
  logger.error('errorHandler middleware encountered error:', error);

  return next(error);
};
