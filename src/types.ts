import { Document } from 'mongoose';
import { Role } from './utils/rbac';

export interface AccessTokenPayload {
  id: string;
  name: string;
  username: string;
  roles: Array<Role>;
}

export interface CaseDocument extends Document<string> {
  title: string;
  description: string;
  fileCategories: Array<string>;
  keywords: Array<string>;
  owners: Array<string>;
  participants: string;
  pedagogicFields: Array<string>;
  problemAreas: Array<string>;
  published: boolean;
  fileAccessAllowedRoles: Array<Role>;
}

export interface CaseFile {
  path: string;
  size: number;
}

export interface FulltextSearchResult {
  case: CaseDocument;
  score: number;
}

export interface UserDocument extends Document<string> {
  cases: Array<string>;
  emailVerified: boolean;
  emailVerificationCode: string;
  resetPasswordCode: string;
  name: string;
  username: string;
  passwordHash: string;
  sessions: Array<UserSession>;
  roles: Array<Role>;
}

export interface UserSession {
  refreshToken: string;
  lastAccessTimestamp: Date;
  loginTimestamp: Date;
  userAgent: string;
}
