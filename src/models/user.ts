/**
 * Mongoose model for the "User" collection.
 */

import mongoose from 'mongoose';

import { UserDocument, UserSession } from '../types';
import { Role } from '../utils/rbac';

const userSessionSchema = new mongoose.Schema<UserSession>({
  refreshToken: {
    type: String,
    required: true,
  },
  lastAccessTimestamp: {
    type: Date,
  },
  loginTimestamp: {
    type: Date,
  },
  userAgent: {
    type: String,
  },
});

const userSchema = new mongoose.Schema<UserDocument>({
  cases: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Case',
    },
  ],
  emailVerified: {
    type: Boolean,
    default: false,
  },
  emailVerificationCode: {
    type: String,
  },
  resetPasswordCode: {
    type: String,
  },
  name: {
    type: String,
  },
  username: {
    type: String,
    minlength: 3,
    required: true,
    unique: true,
  },
  passwordHash: {
    type: String,
    required: true,
  },
  roles: {
    type: [String],
    enum: Object.values(Role),
    default: [],
  },
  sessions: {
    type: [userSessionSchema],
  },
});

userSchema.set('toJSON', {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  transform: (_document: any, returnedObject: any) => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/no-unsafe-call
    returnedObject.id = returnedObject._id.toString();
    delete returnedObject._id;
    delete returnedObject.__v;
    delete returnedObject.emailVerificationCode;
    delete returnedObject.forgotPasswordCode;
    delete returnedObject.passwordHash;
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    returnedObject.sessions.map(
      (session: { _id?: string; refreshToken?: string }) => {
        delete session._id;
        delete session.refreshToken;
      },
    );
  },
});

export default mongoose.model<UserDocument>('User', userSchema);
