/**
 * Mongoose model for the "Case" collection.
 */

import mongoose from 'mongoose';
import { CaseDocument } from '../types';
import { Role } from '../utils/rbac';

const caseSchema = new mongoose.Schema<CaseDocument>({
  title: {
    type: String,
    minlength: 3,
    required: true,
    unique: true,
  },
  description: {
    type: String,
  },
  fileCategories: {
    type: [String],
  },
  keywords: {
    type: [String],
  },
  owners: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
  ],
  participants: {
    type: String,
  },
  pedagogicFields: {
    type: [String],
  },
  problemAreas: {
    type: [String],
  },
  published: {
    type: Boolean,
    default: false,
  },
  fileAccessAllowedRoles: {
    type: [String],
    enum: Object.values(Role),
    default: Object.values(Role),
  },
});

caseSchema.index({ '$**': 'text' });

caseSchema.set('toJSON', {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  transform: (_document: any, returnedObject: any) => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/no-unsafe-call
    returnedObject.id = returnedObject._id.toString();
    delete returnedObject._id;
    delete returnedObject.__v;
  },
});

export default mongoose.model<CaseDocument>('Case', caseSchema);
