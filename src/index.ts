import app from './app';
import http from 'http';
import logger from './utils/logger';
import config from 'config';

const PORT: number = config.get('server.port');
const server = http.createServer(app);

server.listen(PORT, () => {
  logger.info(`Server running on port ${PORT}`);
});
