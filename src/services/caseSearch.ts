/**
 * Abstraction layer for the "cases" index in Elasticsearch
 */
import config from 'config';
import { Client, errors as ElasticsearchErrors } from '@elastic/elasticsearch';
import Case from '../models/case';
import logger from '../utils/logger';
import { CaseDocument } from '../types';

const elasticsearchUrl = config.get<string>('elasticsearch.url');
const casesIndex = config.get<string>('elasticsearch.casesIndex');

const addCaseToIndex = async (newCase: CaseDocument): Promise<void> => {
  await elasticSearchClient.index({
    id: newCase._id,
    index: casesIndex,
    body: { ...newCase.toJSON() },
    refresh: true,
  });
};

/**
 * Removes all documents from the search index. Useful in testing.
 */
const clearIndex = async (): Promise<void> => {
  try {
    await elasticSearchClient.deleteByQuery({
      index: casesIndex,
      conflicts: 'proceed',
      body: { query: { match_all: {} } },
    });
  } catch (error) {
    if (
      error instanceof ElasticsearchErrors.ResponseError &&
      error.message === 'index_not_found_exception'
    ) {
      // no index means there is nothing to clear in the first place
      return;
    }
    throw error;
  }
};

const deleteCaseFromIndex = async (caseId: string): Promise<void> => {
  try {
    await elasticSearchClient.delete({
      id: caseId,
      index: casesIndex,
      refresh: true,
    });
  } catch (error) {
    if (
      error instanceof ElasticsearchErrors.ResponseError &&
      (error.message === 'index_not_found_exception' ||
        error.statusCode === 404)
    ) {
      return; // There is nothing to delete
    }
    throw error;
  }
};

const ensureIndexExists = async (): Promise<void> => {
  try {
    await elasticSearchClient.indices.create({
      index: casesIndex,
      body: {
        settings: {
          analysis: {
            analyzer: {
              default: {
                tokenizer: 'standard',
                filter: ['german_stop_words_filter'],
              },
            },
            filter: {
              german_stop_words_filter: {
                type: 'stop',
                ignore_case: true,
                stopwords: ['_german_'],
              },
            },
          },
        },
      },
    });
    logger.info('Elasticsearch cases index created.');
  } catch (error) {
    if (
      error instanceof ElasticsearchErrors.ResponseError &&
      error.message.startsWith('resource_already_exists_exception')
    ) {
      logger.info('Elasticsearch cases index exists.');
    } else if (error instanceof Error) {
      logger.error(
        'Failed to create Elasticsearch cases index.',
        error.message,
      );
    }
  }
};

const searchAdvanced = async (
  term: string | undefined,
  fileCategories: Array<string>,
  pedagogicFields: Array<string>,
  problemAreas: Array<string>,
  includeUnpublished = false,
): Promise<{
  numHits: { value: number; relation: string };
  hits: Array<CaseDocument>;
}> => {
  // build the "must" section of the elasticsearch query
  const must = [];
  if (term) {
    must.push({
      multi_match: {
        query: term,
        fields: ['title', 'description', 'keywords', 'participants'],
        fuzziness: '1',
      },
    });
  }
  if (fileCategories.length > 0) {
    must.push({ query_string: { query: fileCategories.join(' AND ') } });
  }
  if (pedagogicFields.length > 0) {
    must.push({ query_string: { query: pedagogicFields.join(' AND ') } });
  }
  if (problemAreas.length > 0) {
    must.push({ query_string: { query: problemAreas.join(' AND ') } });
  }

  const response = await elasticSearchClient.search({
    index: casesIndex,
    body: {
      size: 50,
      query: {
        bool: {
          filter: includeUnpublished
            ? { match_all: {} }
            : { match: { published: { query: true } } },
          must: must,
        },
      },
      highlight: {
        fields: {
          title: {},
          description: {},
          fileCategories: {},
          keywords: {},
          participants: {},
          pedagogicFields: {},
          problemAreas: {},
        },
      },
    },
  });

  if (response.body.hits.hits.length === 0) {
    return { numHits: { value: 0, relation: 'eq' }, hits: [] };
  }

  // The Elasticsearch type definitions "are not 100% complete yet".
  // Source: https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/current/typescript.html
  // I can't be bothered to write custom definitions when its just a matter of time until this is sorted out upstream. Hence the disabled linter rules for the time being.
  /* eslint-disable @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-return, @typescript-eslint/no-explicit-any */
  return {
    numHits: response.body.hits.total,
    hits: response.body.hits.hits.map((hit: any) => {
      return { case: hit._source, highlight: hit.highlight, score: hit._score };
    }),
  };
  /* eslint-enable @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-return, @typescript-eslint/no-explicit-any*/
};

const searchSimple = async (
  term: string,
  offset = 0,
  limit = 10,
  includeUnpublished = false,
): Promise<{
  numHits: { value: number; relation: string };
  offset: number;
  limit: number;
  hits: Array<CaseDocument>;
}> => {
  const params = {
    index: casesIndex,
    body: {
      from: offset,
      size: limit,
      query: {
        bool: {
          filter: includeUnpublished
            ? { match_all: {} }
            : { match: { published: { query: true } } },
          must: [
            {
              bool: {
                should: [
                  {
                    multi_match: {
                      query: term,
                      fields: [
                        'title',
                        'description',
                        'fileCategories',
                        'keywords',
                        'participants',
                        'pedagogicFields',
                        'problemAreas',
                      ],
                      fuzziness: '1',
                    },
                  },
                  {
                    query_string: {
                      query: `*${term}*`,
                      fields: [
                        'title',
                        'description',
                        'fileCategories',
                        'keywords',
                        'participants',
                        'pedagogicFields',
                        'problemAreas',
                      ],
                    },
                  },
                ],
              },
            },
          ],
        },
      },
      highlight: {
        fields: {
          title: {},
          description: {},
          fileCategories: {},
          keywords: {},
          participants: {},
          pedagogicFields: {},
          problemAreas: {},
        },
        number_of_fragments: 0,
        require_field_match: false,
      },
    },
  };

  const response = await elasticSearchClient.search(params);

  if (response.body.hits.hits.length === 0) {
    return { numHits: { value: 0, relation: 'eq' }, offset, limit, hits: [] };
  }

  // The Elasticsearch type definitions "are not 100% complete yet".
  // Source: https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/current/typescript.html
  // I can't be bothered to write custom definitions when its just a matter of time until this is sorted out upstream. Hence the disabled linter rules for the time being.
  /* eslint-disable @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-return, @typescript-eslint/no-explicit-any */
  return {
    numHits: response.body.hits.total,
    offset,
    limit,
    hits: response.body.hits.hits.map((hit: any) => {
      return { case: hit._source, highlight: hit.highlight, score: hit._score };
    }),
  };
  /* eslint-enable @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-return, @typescript-eslint/no-explicit-any*/
};

const stats = async (): Promise<Record<string, number>> => {
  const response = await elasticSearchClient.indices.stats({
    index: casesIndex,
    level: 'indices',
    metric: 'docs',
  });
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
  return response.body.indices[casesIndex].total.docs;
};

/**
 * Ensures that the cases in MongoDB and Elasticsearch are identical.
 * Deletes existing index and rebuilds it.
 */
const synchronize = async (): Promise<void> => {
  logger.info('Elasticsearch synchronization started');

  await clearIndex();
  const cases = await Case.find({});

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const body: Array<Record<string, any>> = [];
  cases.map((c) => {
    body.push({ index: { _index: casesIndex, _id: c._id } });
    body.push(c.toJSON());
  });

  await elasticSearchClient.bulk({ index: casesIndex, body, refresh: true });
  logger.info('Elasticsearch synchronization finished');
};

const updateCaseInIndex = async (updatedCase: CaseDocument): Promise<void> => {
  await elasticSearchClient.index({
    id: updatedCase._id,
    index: casesIndex,
    body: { ...updatedCase.toJSON() },
    refresh: true,
  });
};

const elasticSearchClient = new Client({
  node: elasticsearchUrl,
  maxRetries: 1,
  requestTimeout: 5000,
});
void ensureIndexExists();

export default {
  addCaseToIndex,
  clearIndex,
  deleteCaseFromIndex,
  ensureIndexExists,
  searchAdvanced,
  searchSimple,
  stats,
  synchronize,
  updateCaseInIndex,
};
