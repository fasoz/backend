import config from 'config';
import logger from '../utils/logger';
import nodemailer from 'nodemailer';

let transporter: nodemailer.Transporter;
let usingTestAccount = false;

const from: string = config.get('email.from');

const initialize = async () => {
  const emailServerConfig: Record<string, string | Record<string, string>> =
    config.get('email.server');

  if (!emailServerConfig?.host) {
    usingTestAccount = true;

    const testAccount = await nodemailer.createTestAccount();
    transporter = nodemailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false,
      auth: {
        user: testAccount.user,
        pass: testAccount.pass,
      },
    });
  } else {
    transporter = nodemailer.createTransport(emailServerConfig);
  }
};
initialize().catch(logger.error);

const sendMail = async (
  to: Array<string>,
  cc: Array<string> = [],
  bcc: Array<string> = [],
  subject: string,
  body: string,
  replyTo?: string,
): Promise<void> => {
  if (process.env.NODE_ENV === 'test') return;

  try {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const sentMessageInfo = await transporter.sendMail({
      from,
      to,
      cc,
      bcc,
      subject,
      html: body,
      replyTo,
    });
    logger.info(`Email sent to '${to.join()}' with subject '${subject}'`);
    if (usingTestAccount) {
      const previewUrl = nodemailer.getTestMessageUrl(sentMessageInfo);
      if (previewUrl) logger.info(`Email preview: ${previewUrl}`);
    }
  } catch (error) {
    if (error instanceof Error) {
      logger.error(
        `Sending email '${to.join()}' with subject '${subject}' failed:`,
        error,
      );
    }
  }
};

export default {
  sendMail,
};
