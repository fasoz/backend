/**
 * Provides an abstracted interface for the Minio S3 file storage
 */

import config from 'config';
import Stream from 'stream';
import { CaseFile } from '../types';
import minioClient from '../utils/minioClient';
import * as types from '../types';
import { UploadedObjectInfo } from 'minio';
import he from 'he';

const caseFilesBucket: string = config.get('minio.caseFilesBucket');

/**
 * Deletes a file from a case.
 * @param caseId
 * @param path
 */
const deleteFile = (caseId: string, path: string): Promise<void> => {
  return minioClient.removeObject(caseFilesBucket, `${caseId}/${path}`);
};

/**
 * Get a readable file stream for a specific case file.
 * @param caseId
 * @param path
 */
const fetchFile = (caseId: string, path: string): Promise<Stream.Readable> => {
  const passThrough = new Stream.PassThrough();
  return new Promise((resolve, reject) => {
    minioClient
      .getObject(caseFilesBucket, `${caseId}/${path}`)
      .then((minioStream) => {
        minioStream.pipe(passThrough);
        resolve(passThrough);
      })
      .catch((error) => reject(error));
  });
};

const listFiles = (caseId: string): Promise<Array<CaseFile>> => {
  return new Promise<Array<CaseFile>>((resolve, reject) => {
    const caseFiles: Array<types.CaseFile> = [];

    const objectsStream = minioClient.listObjects(
      caseFilesBucket,
      caseId,
      true,
    );

    objectsStream.on('data', (object) => {
      const pathWithoutCaseIdPrefix = object.name.substr(caseId.length + 1);
      const decodedPath = he.decode(pathWithoutCaseIdPrefix); // Minio HTML encodes file names
      caseFiles.push({
        path: decodedPath,
        size: object.size,
      });
    });

    objectsStream.on('error', (error) => {
      return reject(error);
    });

    objectsStream.on('end', () => {
      return resolve(caseFiles);
    });
  });
};

/**
 * Save a file to a case.
 * @param caseId
 * @param path
 * @param fileStream
 */
const storeFile = (
  caseId: string,
  path: string,
  fileStream: Stream.Readable,
): Promise<UploadedObjectInfo> => {
  return minioClient.putObject(
    caseFilesBucket,
    `${caseId}/${path}`,
    fileStream,
  );
};

/**
 * Deletes all files for the specified case.
 * @param caseId
 */
const deleteCase = async (caseId: string): Promise<void> => {
  const objects = (await listFiles(caseId)).map(
    (caseFile) => `${caseId}/${caseFile.path}`,
  );
  return minioClient.removeObjects(caseFilesBucket, objects);
};

/**
 * Deletes all files. Only needed in testing.
 */
const deleteAllCases = async (): Promise<void> => {
  const bucketExists = await minioClient.bucketExists(caseFilesBucket);
  if (!bucketExists) {
    return Promise.resolve();
  }

  return new Promise((resolve, reject) => {
    const files: Array<string> = [];

    const objectsStream = minioClient.listObjects(caseFilesBucket, '/', true);

    objectsStream.on('data', (object) => {
      files.push(object.name);
    });

    objectsStream.on('error', (error) => {
      reject(error);
    });

    objectsStream.on('end', () => {
      minioClient.removeObjects(caseFilesBucket, files, (error) => {
        if (error) reject(error);
        else resolve();
      });
    });
  });
};

export default {
  deleteFile,
  deleteAllCases,
  fetchFile,
  listFiles,
  storeFile,
  deleteCase,
};
