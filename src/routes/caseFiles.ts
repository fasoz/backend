import express from 'express';
import archiver from 'archiver';
import Busboy from 'busboy';
import Stream from 'stream';
import { UploadedObjectInfo } from 'minio';
import * as types from '../types';

import { requiresPermission } from '../middlewares/permissions';
import {
  checkCaseUpdatePermissions,
  checkFileAccessRoleRequirements,
} from '../middlewares/cases';

import Case from '../models/case';

import caseFileStorage from '../services/caseFileStorage';

import logger from '../utils/logger';
import { Permission } from '../utils/rbac';

const router = express.Router();

router.get(
  '/:id/file',
  requiresPermission(Permission.CaseFilesList),
  checkFileAccessRoleRequirements(),
  async (request, response) => {
    const caseId = request.params.id; // determine case ID from URL parameter
    const caseFiles = await caseFileStorage.listFiles(caseId); // fetch list of files belonging to this case
    response.json(caseFiles);
  },
);

router.get(
  '/:id/files.zip',
  requiresPermission(Permission.CaseFilesDownload),
  checkFileAccessRoleRequirements(),
  async (request, response) => {
    const caseId = request.params.id;
    const caseFiles = await caseFileStorage.listFiles(caseId); // fetch list of files belonging to this case

    // create ZIP archive
    const archive = archiver('zip');

    // instruct Express.js to start piping the ZIP archive into the HTTP response
    response.attachment(`${caseId}.zip`);
    archive.pipe(response);

    // stream case files to the ZIP archive (which pipes it through to the HTTP response)
    for (const file of caseFiles) {
      const fileStream = await caseFileStorage.fetchFile(caseId, file.path);
      archive.append(fileStream, { name: file.path });
    }

    // end the stream
    await archive.finalize();
  },
);

router.delete(
  '/:id/file/*',
  checkCaseUpdatePermissions(),
  checkFileAccessRoleRequirements(),
  async (request, response) => {
    const caseId = request.params.id;
    const fileName = request.params[0];
    await caseFileStorage.deleteFile(caseId, fileName);
    response.status(204).end();
  },
);

router.get(
  '/:id/file/*',
  requiresPermission(Permission.CaseFilesDownload),
  checkFileAccessRoleRequirements(),
  async (request, response) => {
    const caseId = request.params.id;
    const fileName = request.params[0];
    try {
      const fileStream = await caseFileStorage.fetchFile(caseId, fileName);
      response.attachment(fileName);
      fileStream.pipe(response);
    } catch (_error) {
      response.status(404).json({ error: 'file not found' });
      return;
    }
  },
);

router.post(
  '/:id/file*',
  checkCaseUpdatePermissions(),
  checkFileAccessRoleRequirements(),
  async (request, response) => {
    const caseId = request.params.id;
    const folder = request.params[0] ? request.params[0].substr(1) : ''; // strip leading slash
    const thisCase: types.CaseDocument | null = await Case.findById(caseId);

    if (!thisCase) {
      response.status(404).json({ error: 'Case ID not found' });
      return;
    }

    // prepare array of promises for uploaded files that we can later await
    const storeFilePromises: Array<Promise<UploadedObjectInfo>> = [];

    // start processing the individual uploaded files
    const busboy = new Busboy({ headers: request.headers });
    busboy.on(
      'file',
      (_fieldName, fileStream, fileName, _encoding, mimetype) => {
        const path = folder ? `${folder}/${fileName}` : fileName;

        const passThrough = new Stream.PassThrough(); // passthrough stream for compatibility between busboy and MinIO
        fileStream.pipe(passThrough); // pipe busboy file stream into passthrough stream
        const promise = caseFileStorage.storeFile(caseId, path, passThrough); // store file in MinIO
        storeFilePromises.push(promise); // add promise to array so we can await it later

        fileStream.on('end', () => {
          logger.info(
            `File '${fileName}' (${mimetype}) uploaded for case ${caseId}`,
          );
        });
      },
    );

    // after all uploaded files have been processed
    busboy.on('finish', () => {
      // wait for file processing promises to resolve
      Promise.all(storeFilePromises)
        .then(() => {
          response.send({ message: 'file uploaded' });
        })
        .catch((error) => logger.error(error));
    });

    // now that the plumbing is set up, start actually piping the incoming files into busboy
    request.pipe(busboy);
  },
);

export default router;
