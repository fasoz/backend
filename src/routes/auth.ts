/**
 * Endpoints for obtaining access and refresh tokens.
 */

import bcrypt from 'bcryptjs';
import config from 'config';
import cookieParser from 'cookie-parser';
import express from 'express';
import * as yup from 'yup';
import jwt from 'jsonwebtoken';
import User from '../models/user';
import { AccessTokenPayload, UserDocument, UserSession } from '../types';
import { requestValidation } from '../middlewares/validation';

const jwtSecret: string = config.get('jwt.secret');

const refreshTokenCookieOptions: express.CookieOptions = {
  httpOnly: true,
  path: '/api/auth/',
  sameSite: 'strict',
};

interface RefreshToken {
  username: string;
  id: string;
  iat: number;
}

const generateAccessToken = (user: UserDocument) => {
  const accessTokenPayload: AccessTokenPayload = {
    id: user._id ?? '',
    name: user.name,
    username: user.username,
    roles: user.roles,
  };
  const accessToken = jwt.sign(accessTokenPayload, jwtSecret, {
    expiresIn: '15m',
  });

  return { accessToken, accessTokenPayload };
};

const generateRefreshToken = (user: UserDocument) => {
  const refreshTokenPayload = {
    id: user._id,
  };
  const refreshToken = jwt.sign(refreshTokenPayload, jwtSecret);

  return { refreshToken, refreshTokenPayload };
};

const router = express.Router();
router.use(cookieParser());

const loginValidationSchema = yup.object({
  username: yup.string().required().email(),
  password: yup.string().required(),
});

/**
 * Validates the login credentials. Sets a http cookie with a long-lived refresh token and returns short-lived access tokens.
 */
router.post(
  '/login',
  requestValidation(loginValidationSchema),
  async (request, response) => {
    const requestBody = loginValidationSchema.validateSync(request.body);

    const user = await User.findOne({ username: requestBody.username });
    const passwordCorrect =
      user === null
        ? false
        : bcrypt.compareSync(requestBody.password, user.passwordHash);

    if (!(user && passwordCorrect)) {
      response.status(401).json({
        error: 'invalid username or password',
      });
      return;
    }

    if (!user.emailVerified) {
      response.status(401).json({
        error:
          'Account has not been activated yet. Email verification pending.',
      });
      return;
    }

    const { accessToken, accessTokenPayload } = generateAccessToken(user);
    const { refreshToken } = generateRefreshToken(user);

    user.sessions.push({
      refreshToken,
      lastAccessTimestamp: new Date(),
      loginTimestamp: new Date(),
      userAgent: request.headers['user-agent'] || '',
    });

    await user.save();

    response
      .status(200)
      .cookie('refresh-token', refreshToken, {
        expires: new Date(Date.now() + 365.25 * 24 * 3600000),
        ...refreshTokenCookieOptions,
      })
      .json({
        token: accessToken,
        ...accessTokenPayload,
      });
  },
);

/**
 * Deletes the refresh token.
 */
router.get('/logout', async (request, response) => {
  const cookies = request.cookies as Record<string, string>;
  const encodedRefreshToken = cookies['refresh-token'];

  if (!encodedRefreshToken) {
    response.json({ message: 'already logged out' });
    return;
  }

  const user = await User.findOne({
    'sessions.refreshToken': encodedRefreshToken,
  });
  if (user) {
    user.sessions = user.sessions.filter(
      (session) => session.refreshToken !== encodedRefreshToken,
    );
    await user.save();
  }

  response
    .clearCookie('refresh-token', refreshTokenCookieOptions)
    .json({ message: 'logout successful' });
});

/**
 * Rotates the refresh token and returns a short-lived access token. Does nothing if there is no refresh token.
 */
router.get('/verify_session', async (request, response) => {
  const cookies = request.cookies as Record<string, string>;
  const encodedRefreshToken = cookies['refresh-token'];
  if (!encodedRefreshToken) {
    // no refresh token => user not logged in => nothing to do
    response.status(204).end();
    return;
  }

  let decodedRefreshToken: RefreshToken;
  try {
    decodedRefreshToken = jwt.verify(
      encodedRefreshToken,
      jwtSecret,
    ) as RefreshToken;
  } catch (error) {
    response
      .status(401)
      .clearCookie('refresh-token', refreshTokenCookieOptions)
      .json({ error: 'invalid refresh token' });
    return;
  }

  const user = await User.findById(decodedRefreshToken.id);
  if (!user) {
    response
      .status(401)
      .clearCookie('refresh-token', refreshTokenCookieOptions)
      .json({ error: 'invalid user' });
    return;
  }

  const thisSession = user.sessions.find(
    (session: UserSession) => session.refreshToken === encodedRefreshToken,
  );
  if (!thisSession) {
    response
      .status(401)
      .json({ error: 'invalid refresh token or session revoked' });
    return;
  }

  const { accessToken, accessTokenPayload } = generateAccessToken(user);
  const { refreshToken } = generateRefreshToken(user);

  thisSession.refreshToken = refreshToken;
  thisSession.lastAccessTimestamp = new Date();
  thisSession.userAgent = request.headers['user-agent'] || '';
  await user.save();

  response
    .status(200)
    .cookie('refresh-token', refreshToken, {
      expires: new Date(Date.now() + 365.25 * 24 * 3600000),
      ...refreshTokenCookieOptions,
    })
    .json({
      token: accessToken,
      ...accessTokenPayload,
    });
});

export default router;
