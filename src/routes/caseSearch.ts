import express from 'express';
import * as yup from 'yup';
import { requestValidation } from '../middlewares/validation';
import { Permission } from '../utils/rbac';
import caseSearch from '../services/caseSearch';

const router = express.Router();

export const caseSearchValidationSchema = yup.object({
  q: yup.string().required(),
  offset: yup.number().default(0).min(0),
  limit: yup.number().default(10).min(10).max(100),
});

router.post(
  '/simple',
  requestValidation(caseSearchValidationSchema),
  async (request, response) => {
    const requestBody = caseSearchValidationSchema.validateSync(request.body);

    const includeUnpublished = request.userPermissions.includes(
      Permission.CasesListUnpublished,
    );

    const cases = await caseSearch.searchSimple(
      requestBody.q,
      requestBody.offset,
      requestBody.limit,
      includeUnpublished,
    );
    response.status(200).json(cases);
  },
);

export const caseAdvancedSearchValidationSchema = yup.object({
  term: yup.string(),
  fileCategories: yup.array().of(yup.string().ensure()).ensure(),
  pedagogicFields: yup.array().of(yup.string().ensure()).ensure(),
  problemAreas: yup.array().of(yup.string().ensure()).ensure(),
});

router.post(
  '/advanced',
  requestValidation(caseAdvancedSearchValidationSchema),
  async (request, response) => {
    const requestBody = caseAdvancedSearchValidationSchema.validateSync(
      request.body,
    );

    const includeUnpublished = request.userPermissions.includes(
      Permission.CasesListUnpublished,
    );

    const results = await caseSearch.searchAdvanced(
      requestBody.term,
      requestBody.fileCategories,
      requestBody.pedagogicFields,
      requestBody.problemAreas,
      includeUnpublished,
    );

    response.status(200).json(results);
  },
);

export default router;
