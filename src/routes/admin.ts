import express from 'express';
import caseSearch from '../services/caseSearch';
import { Role } from '../utils/rbac';

const router = express.Router();

// router middleware that only permits logged in users with Role 'Administrator"
router.use((request, response, next) => {
  if (request.accessToken?.roles.includes(Role.Administrator)) {
    next();
    return;
  }
  response.status(403).end();
});

// endpoints
router.get('/elasticsearch/stats', async (_request, response) => {
  const stats = await caseSearch.stats();
  response.status(200).json(stats);
});

router.post('/elasticsearch/rebuild_index', async (_request, response) => {
  await caseSearch.synchronize();
  response.status(200).end();
});

export default router;
