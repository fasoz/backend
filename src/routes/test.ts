/**
 * endpoints for end to end testing.
 * !!! WARNING: do not load this router in production !!!
 */

import express from 'express';
import Case from '../models/case';
import User from '../models/user';
import caseFileStorage from '../services/caseFileStorage';
import logger from '../utils/logger';
import { hashPassword } from './users';
import { Role } from '../utils/rbac';
import caseSearch from '../services/caseSearch';

const router = express.Router();

/**
 * Middleware that checks if this router is loaded in a production environment. Returns 404 if necessary.
 * @param _request
 * @param response
 * @param next
 */
const checkEnvironment = (
  _request: express.Request,
  response: express.Response,
  next: express.NextFunction,
) => {
  if (
    process.env.NODE_ENV === 'prod' ||
    process.env.NODE_ENV === 'production'
  ) {
    logger.warn('Do not load test router in production environments!');
    response.status(404).end();
  }
  next();
};
router.use(checkEnvironment);

/**
 * Empties the MongoDB database and Minio S3 file storage.
 */
router.post('/reset', async (_request, response) => {
  await Promise.all([
    Case.deleteMany({}),
    User.deleteMany({}),
    caseFileStorage.deleteAllCases(),
    caseSearch.ensureIndexExists().then(() => caseSearch.clearIndex()),
  ]);
  response.status(204).end();
});

/**
 * Creates a verified user. By default this user has full admin rights. This endpoint is useful for manual testing during development. It also used for our end-to-end tests.
 */
router.post('/create_admin', async (request, response) => {
  const requestBody = request.body as {
    name: string;
    username: string;
    password: string;
    roles: Array<string>;
  };

  const user = new User({
    emailVerified: true,
    name: requestBody.name ? requestBody.name : 'Ian Root',
    username: requestBody.username ? requestBody.username : 'admin@example.com',
    passwordHash: hashPassword(
      requestBody.password ? requestBody.password : 'admin123456789',
    ),
    roles: requestBody.roles
      ? requestBody.roles
      : [Role.Administrator, Role.Editor, Role.Researcher],
  });
  await user.save();
  response.status(201).end();
});

/**
 * returns the password reset code for a given email
 */
router.get('/reset_password_code/:email', async (request, response) => {
  const user = await User.findOne({ username: request.params.email });
  if (!user) {
    response.status(404).end();
    return;
  }

  response.status(200).json({ test: 'test', code: user.resetPasswordCode });
});

/**
 * Forces the given email address to be marked as verified.
 */
router.post('/verify_email/:email', async (request, response) => {
  const user = await User.findOne({ username: request.params.email });
  if (!user) {
    response.status(404).end();
    return;
  }
  user.emailVerified = true;
  await user.save();
  response.status(204).end();
});

export default router;
