/**
 * CRUD endpoints for cases and case files.
 */

import express from 'express';
import * as yup from 'yup';

import Case from '../models/case';
import User from '../models/user';

import {
  checkCaseReadPermissions,
  checkCaseUpdatePermissions,
} from '../middlewares/cases';
import { requiresPermission } from '../middlewares/permissions';
import { requestValidation } from '../middlewares/validation';

import { Permission, Role } from '../utils/rbac';

import caseFileStorage from '../services/caseFileStorage';
import caseSearch from '../services/caseSearch';

const router = express.Router();

router.get('/', async (request, response) => {
  // include file categories from unpublished cases if the user is allowed to see those
  const queryConditions =
    request.accessToken &&
    request.userPermissions.includes(Permission.CasesListUnpublished)
      ? {}
      : { published: true };

  // fetch all cases from MongoDB
  const cases = await Case.find(queryConditions).sort('title');

  response.send(cases);
});

const caseValidationSchema = yup.object({
  title: yup.string().required().min(3),
  description: yup.string().default(''),
  fileCategories: yup.array().of(yup.string()),
  keywords: yup.array().of(yup.string()),
  participants: yup.string().default(''),
  problemAreas: yup.array().of(yup.string()),
  pedagogicFields: yup.array().of(yup.string()),
  published: yup.boolean(),
  fileAccessAllowedRoles: yup
    .array()
    .default(Object.values(Role))
    .of(yup.mixed().oneOf(Object.values(Role))),
});

router.post(
  '/',
  requiresPermission(Permission.CaseCreate),
  requestValidation(caseValidationSchema),
  async (request, response) => {
    const requestBody = caseValidationSchema.validateSync(request.body);

    // check if the user tries to publish the case without the necessary permission
    if (
      !request.userPermissions.includes(Permission.CasePublishOwn) &&
      requestBody.published
    ) {
      response.status(403).json({
        error:
          'Request denied. You lack the necessary permission to publish a case.',
      });
      return;
    }

    // add case to MongoDB
    const newCase = new Case({
      ...requestBody,
      owners: [request.accessToken?.id],
    });
    const savedCase = await newCase.save();

    // add case to Elasticsearch
    const elasticsearchPromise = caseSearch.addCaseToIndex(savedCase);

    // associate case with the user who created it
    const user = await User.findById(request.accessToken?.id);
    if (!user) {
      response.status(500).json({
        error: 'User associated with this session is not in the database.',
      });
      return;
    }
    if (savedCase._id) user.cases = user.cases.concat(savedCase._id);

    // wait for promises to resolve
    await Promise.all([user.save(), elasticsearchPromise]);

    response.status(201).send(savedCase.toJSON());
  },
);

router.get('/file_categories', async (request, response) => {
  // include file categories from unpublished cases if the user is allowed to see those
  const conditions = request.userPermissions.includes(
    Permission.CasesListUnpublished,
  )
    ? {}
    : { published: true };

  // fetch list of unique file categories from MongoDB
  const fileCategories = await Case.find(conditions).distinct('fileCategories');

  response.json(fileCategories);
});

router.get('/keywords', async (request, response) => {
  // include keywords from unpublished cases if the user is allowed to see those
  const conditions = request.userPermissions.includes(
    Permission.CasesListUnpublished,
  )
    ? {}
    : { published: true };

  // fetch list of unique keywords from MongoDB
  const keywords = await Case.find(conditions).distinct('keywords');

  response.json(keywords);
});

router.get('/problem_areas', async (request, response) => {
  // include problem areas from unpublished cases if the user is allowed to see those
  const conditions = request.userPermissions.includes(
    Permission.CasesListUnpublished,
  )
    ? {}
    : { published: true };

  // fetch list of unique problem areas from MongoDB
  const problemAreas = await Case.find(conditions).distinct('problemAreas');

  response.json(problemAreas);
});

router.get('/pedagogic_fields', async (request, response) => {
  // include pedagogic fields from unpublished cases if the user is allowed to see those
  const conditions = request.userPermissions.includes(
    Permission.CasesListUnpublished,
  )
    ? {}
    : { published: true };

  // fetch list of unique problem areas from MongoDB
  const pedagogicFields = await Case.find(conditions).distinct(
    'pedagogicFields',
  );
  response.json(pedagogicFields);
});

router.get('/:id', checkCaseReadPermissions(), async (request, response) => {
  const caseId = request.params.id; // determine caseId from URL parameter
  const thisCase = await Case.findById(caseId); // fetch case from MongoDB
  response.json(thisCase);
});

router.put(
  '/:id',
  requestValidation(caseValidationSchema),
  checkCaseUpdatePermissions(),
  async (request, response, next) => {
    /* makes sure that a case may only be set to published=true if the user has the necessary permissions */
    const requestBody = caseValidationSchema.validateSync(request.body);

    if (!requestBody.published) {
      next();
      return;
    }

    const caseId = request.params.id;
    const thisCase = await Case.findById(caseId);

    // allow if published field remains unchanged
    if (requestBody.published === thisCase?.published) {
      next();
      return;
    }

    // check if user is allowed to set any case to published = true
    if (request.userPermissions.includes(Permission.CasePublishAny)) {
      next();
      return;
    }

    // check if user owns this case and is allowed to publish own cases
    if (
      thisCase &&
      request.accessToken &&
      thisCase.owners.includes(request.accessToken.id) &&
      request.userPermissions.includes(Permission.CasePublishOwn)
    ) {
      next();
      return;
    }

    response
      .status(403)
      .json({ error: 'Request denied. You lack the necessary permissions.' });
  },
  async (request, response) => {
    const caseId = request.params.id;

    // update case in MongoDB
    const updatedCase = await Case.findByIdAndUpdate(caseId, request.body, {
      new: true,
    });

    if (!updatedCase) {
      response.status(404).json({ error: 'Case ID not found' });
      return;
    }

    // update Elasticsearch index
    await caseSearch.updateCaseInIndex(updatedCase);

    response.json(updatedCase.toJSON());
  },
);

router.delete(
  '/:id',
  async (request, response, next) => {
    /* check if the user has the necessary permission to delete this case */
    // check if user is logged in
    if (!request.accessToken) {
      response.status(401).json({ error: 'access token missing' });
      return;
    }

    // check if user is allowed to delete any case, regardless of ownership
    if (request.userPermissions.includes(Permission.CaseDeleteAny)) {
      next();
      return;
    }

    // check if user tries to delete a case they own
    const caseId = request.params.id;
    const thisCase = await Case.findById(caseId);
    if (
      thisCase &&
      thisCase.owners.includes(request.accessToken.id) &&
      request.userPermissions.includes(Permission.CaseDeleteOwn)
    ) {
      next();
      return;
    }

    // none of the previous checks passed, so the user lacks the necessary permissions
    response
      .status(403)
      .json({ error: 'Request denied. You lack the necessary permissions.' });
  },
  async (request, response) => {
    const caseId = request.params.id;

    await Promise.all([
      Case.findByIdAndRemove(caseId), // delete case from MongoDB
      caseFileStorage.deleteCase(caseId), // delete cases files from MinIO
      caseSearch.deleteCaseFromIndex(caseId), // delete case from Elasticsearch
    ]);

    response.status(204).end();
  },
);

export default router;
