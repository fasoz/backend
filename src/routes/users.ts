/**
 * CRUD endpoints for user management.
 */

import bcrypt from 'bcryptjs';
import config from 'config';
import express from 'express';
import mongoose from 'mongoose';
import * as yup from 'yup';

import User from '../models/user';

import { requiresPermission } from '../middlewares/permissions';
import { requestValidation } from '../middlewares/validation';

import emailTemplates from '../utils/emailTemplates';
import { Permission, Role } from '../utils/rbac';
import logger from '../utils/logger';

import email from '../services/email';

const emailVerificationCodeLength: number = config.get(
  'email.verificationCodeLength',
);
const forgotPasswordCodeLength: number = config.get(
  'email.forgotPasswordCodeLength',
);

const router = express.Router();

const generateRandomString = (length = 32) => {
  const letters = '0123456789abcdefghijklmnopqrstuvwxyz';
  const randomLetters = [];
  for (let i = 0; i < length; i++) {
    randomLetters.push(letters[Math.floor(Math.random() * letters.length)]);
  }
  return randomLetters.join('');
};

export const hashPassword = (password: string): string => {
  const saltRounds: number = config.get('bcrypt.saltRounds');
  return bcrypt.hashSync(password, saltRounds);
};

router.get(
  '/',
  requiresPermission(Permission.UserList),
  async (_request, response) => {
    const users = await User.find({})
      .sort({ name: 1, username: 1 })
      .populate('cases', { id: 1, title: 1 });
    response.json(users.map((u) => u.toJSON()));
  },
);

const createUserValidationSchema = yup.object({
  name: yup.string(),
  username: yup.string().required().email(),
  password: yup.string().required().min(12),
  roles: yup.array().of(yup.mixed().oneOf(Object.values(Role))),
});

router.post(
  '/',
  requestValidation(createUserValidationSchema),
  async (request, response) => {
    const requestBody = createUserValidationSchema.validateSync(request.body);

    if (requestBody.roles && requestBody.roles.length > 0) {
      if (!request.accessToken) {
        response.status(401).json({
          error: 'Access token missing.',
        });
        return;
      }
      if (!request.userPermissions.includes(Permission.UserRolesUpdate)) {
        response.status(403).json({
          error:
            "Request denied. You don't have permission to assign user roles.",
        });
        return;
      }
    } else {
      // if a user has no roles, assign guest role
      requestBody.roles = [Role.Guest];
    }

    const passwordHash = hashPassword(requestBody.password);

    const user = new User({
      emailVerified: false,
      emailVerificationCode: generateRandomString(emailVerificationCodeLength),
      username: requestBody.username,
      name: requestBody.name,
      roles: requestBody.roles,
      passwordHash,
    });

    try {
      const savedUser = await user.save();

      const emailContents = emailTemplates.signUp(
        user.name || user.username,
        user.emailVerificationCode,
      );
      await email.sendMail(
        [user.name ? `${user.name} <${user.username}>` : user.username],
        [],
        [],
        emailContents.subject,
        emailContents.body,
      );

      response.status(201).json(savedUser);
    } catch (error) {
      if (error instanceof mongoose.mongo.MongoError && error.code === 11000) {
        response.status(400).json({ error: 'Username already exists' });
        return;
      }
      response.status(400).json({ error: 'Validation Error' });
    }
  },
);

router.get('/:id', async (request, response) => {
  const paramId = request.params.id;

  if (!request.accessToken) {
    response.status(401).json({ error: 'token missing' });
    return;
  }

  const user = await User.findById(paramId).populate('cases', {
    id: 1,
    title: 1,
    description: 1,
    fileCategories: 1,
    keywords: 1,
    participants: 1,
    pedagogicFields: 1,
    problemAreas: 1,
    published: 1,
  });
  if (!user || !user._id) {
    response.status(404).end();
    return;
  }

  if (
    request.userPermissions.includes(Permission.UserReadFull) ||
    request.accessToken.id === paramId
  ) {
    response.json(user.toJSON());
    return;
  }

  if (request.userPermissions.includes(Permission.UserReadBasic)) {
    response.json({
      id: user._id,
      username: user.username,
    });
    return;
  }

  response
    .status(403)
    .json({ error: 'Request denied. You lack the necessary permission.' });
});

router.delete('/:id', async (request, response) => {
  const paramId = request.params.id;

  if (!request.accessToken) {
    response.status(401).json({ error: 'token missing' });
    return;
  }

  if (
    !request.userPermissions.includes(Permission.UserDeleteAny) &&
    !(
      request.accessToken.id === paramId &&
      request.userPermissions.includes(Permission.UserDeleteSelf)
    )
  ) {
    response
      .status(403)
      .json({ error: 'Request denied. You lack the necessary permission.' });
    return;
  }

  await User.findByIdAndRemove(paramId);
  response.status(204).end();
});

const editUserValidationSchema = yup.object({
  name: yup.string(),
  username: yup.string().required().email(),
  password: yup.string().min(12),
  roles: yup.array().of(yup.mixed().oneOf(Object.values(Role))),
  sessions: yup.array().of(
    yup.object({
      refreshToken: yup.string().required(),
    }),
  ),
});

router.put(
  '/:id',
  requestValidation(editUserValidationSchema),
  async (request, response) => {
    const requestBody = editUserValidationSchema.validateSync(request.body);

    if (!request.accessToken) {
      response.status(401).json({ error: 'token missing' });
      return;
    }

    if (
      !request.userPermissions.includes(Permission.UserUpdateAny) &&
      !(
        request.accessToken.id === request.params.id &&
        request.userPermissions.includes(Permission.UserUpdateSelf) &&
        !requestBody.roles
      )
    ) {
      response
        .status(403)
        .json({ error: 'Request denied. You lack the necessary permission.' });
      return;
    }

    if (requestBody.password) {
      request.body.passwordHash = hashPassword(requestBody.password);
      delete requestBody.password;
    }

    const savedUser = await User.findByIdAndUpdate(
      request.params.id,
      request.body,
      {
        new: true,
      },
    );
    if (!savedUser) {
      response.status(404).json({ error: 'User ID not found' });
      return;
    }
    response.json(savedUser.toJSON());
  },
);

const forgotPasswordValidationSchema = yup.object({
  email: yup.string().required().email(),
});

router.post(
  '/forgot_password',
  requestValidation(forgotPasswordValidationSchema),
  async (request, response) => {
    const userEmail = request.body.email as string;

    const user = await User.findOne({ username: userEmail });
    if (!user) {
      // We don't want to leak information on which users exist and which don't. So this response is identical to a valid request.
      response.status(200).end();
      logger.warn(
        `Password reset requested for non-existing user "${userEmail}"`,
      );
      return;
    }

    logger.info(`Password reset requested for email "${userEmail}"`);

    user.resetPasswordCode = generateRandomString(forgotPasswordCodeLength);
    await user.save();

    const emailContents = emailTemplates.forgotPassword(user.resetPasswordCode);
    await email.sendMail(
      [user.username],
      [],
      [],
      emailContents.subject,
      emailContents.body,
    );

    response.status(200).end();
  },
);

const resetPasswordValidationSchema = yup.object({
  code: yup
    .string()
    .required()
    .min(forgotPasswordCodeLength)
    .max(forgotPasswordCodeLength),
  password: yup.string().required().min(12),
});

router.post(
  '/reset_password',
  requestValidation(resetPasswordValidationSchema),
  async (request, response) => {
    const user = await User.findOne({
      resetPasswordCode: request.body.code as string,
    });

    if (!user) {
      response
        .status(400)
        .json({ error: 'invalid email address or reset code' });
      return;
    }

    user.emailVerified = true;
    user.passwordHash = hashPassword(request.body.password as string);
    user.resetPasswordCode = '';

    await user.save();
    response.status(200).json({ message: 'password changed' });
  },
);

router.post('/verify_email/:code', async (request, response) => {
  const verificationCode = request.params.code;

  if (verificationCode.length < emailVerificationCodeLength) {
    response.status(400).json({ error: 'verification code invalid' });
    return;
  }

  const user = await User.findOne({ emailVerificationCode: verificationCode });

  if (!user) {
    response
      .status(404)
      .json({ error: 'user not found or verification code invalid' });
    return;
  }

  if (user.emailVerified) {
    response.json({ message: 'email address already verified' });
    return;
  }

  user.emailVerified = true;
  await user.save();
  response.json({ message: 'email address verified' });
});

export default router;
