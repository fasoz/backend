/**
 * Endpoint used by Kubernetes for its liveness and readiness probe.
 */

import config from 'config';
import express from 'express';
import mongoose from 'mongoose';
import minioClient from '../utils/minioClient';
import logger from '../utils/logger';

const caseFilesBucket: string = config.get('minio.caseFilesBucket');

const router = express.Router();

router.get('/', async (_request, response) => {
  let s3bucketExists: boolean;
  try {
    s3bucketExists = await minioClient.bucketExists(caseFilesBucket);
  } catch (error) {
    if (error instanceof Error)
      logger.error('Health check failed.', error.message);
    response.status(500).end();
    return;
  }

  if (mongoose.connection.readyState !== 1 || !s3bucketExists) {
    response.status(500).end();
    return;
  }

  response.status(200).end();
});

export default router;
