import config from 'config';
import express from 'express';
import * as yup from 'yup';
import { requestValidation } from '../middlewares/validation';
import email from '../services/email';

const contactFormAddresses: Array<string> = config.get(
  'email.contactFormAddresses',
);

const router = express.Router();

const postValidationSchema = yup.object({
  name: yup.string(),
  email: yup.string().email().required(),
  subject: yup.string(),
  message: yup.string().required(),
});

router.post(
  '/',
  requestValidation(postValidationSchema),
  async (request, response) => {
    if (contactFormAddresses.length === 0) {
      response
        .status(503)
        .json({ error: 'Configuration error: no recipient(s) configured' });
      return;
    }

    const body = postValidationSchema.validateSync(request.body);

    const replyTo = body.name ? `${body.name} <${body.email}>` : body.email;
    const subject = body.subject ? body.subject : 'Ohne Betreff';
    const message = `Diese Email wurde über das Kontaktformular des <i>Fallarchivs Soziale Arbeit</i> verschickt.<br />
<br />
${body.message.replace(/(\r\n|\r|\n)/g, '<br />')}`;

    await email.sendMail(
      contactFormAddresses,
      [],
      [],
      subject,
      message,
      replyTo,
    );

    response.json({
      sender: replyTo,
      subject: subject,
      message,
    });
  },
);

export default router;
