/**
 * Basic abstraction layer for console logging.
 * This file should be the only file in the codebase that directly calls console.log()'.
 */

/* eslint-disable no-console */
const info = (...params: Array<string | Error>): void => {
  console.log(...params);
};

const warn = (...params: Array<string | Error>): void => {
  console.warn(...params);
};

const error = (...params: Array<string | Error>): void => {
  console.error(...params);
};

export default {
  info,
  warn,
  error,
};
