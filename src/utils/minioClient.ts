/**
 * Connects to the Minio S3 storage backend.
 * The exported minioClient should only be used through an abstraction layer.
 */

import * as minio from 'minio';
import config from 'config';
import logger from './logger';

const minioConfig: minio.ClientOptions = config.get('minio');
const caseFilesBucket: string = config.get('minio.caseFilesBucket');
const minioClient = new minio.Client(minioConfig);

const connectToMinio = async () => {
  logger.info('Connecting to Minio S3 server at', minioConfig.endPoint);
  try {
    await minioClient.makeBucket(caseFilesBucket, '');
    logger.info(`Created S3 bucket '${caseFilesBucket}'`);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  } catch (error: any) {
    if (error.code === 'BucketAlreadyOwnedByYou') {
      logger.info(`Using existing S3 bucket '${caseFilesBucket}'`);
    } else if (error.code === 'ECONNREFUSED') {
      logger.error('Unable to connect to Minio.');
      process.exit(-1);
    } else {
      logger.error('Minio error.', error);
      process.exit(-1);
    }
  }
};
void connectToMinio();

export default minioClient;
