import config from 'config';

const baseUrl: string = config.get('server.baseUrl');

interface EmailTemplate {
  subject: string;
  body: string;
}

const forgotPassword = (resetPasswordCode: string): EmailTemplate => {
  const resetPasswordLink = `${baseUrl}/passwort_zurücksetzen/${resetPasswordCode}`;
  return {
    subject: 'Passwort zurücksetzen',
    body: `Klicken Sie auf den folgenden Link um ein neues Passwort für Ihr Nutzer*innenkonto zu vergeben:
<br />
<a href="${resetPasswordLink}">${resetPasswordLink}</a>`,
  };
};

const signUp = (name: string, emailVerificationCode: string): EmailTemplate => {
  const emailVerificationLink = `${baseUrl}/email_bestätigen/${emailVerificationCode}`;
  return {
    subject: 'Willkommen im Fallarchiv Soziale Arbeit',
    body: `Hallo ${name},<br />
<br />
willkommen im Fallarchiv Soziale Arbeit.<br />
<br />
Bitte bestätigen Sie ihre Email-Adresse mit dem folgenden Link:<br />
<a href="${emailVerificationLink}">${emailVerificationLink}</a>`,
  };
};

export default {
  forgotPassword,
  signUp,
};
