import config from 'config';
import mongoose from 'mongoose';
import logger from './logger';

const mongoUri: string = config.get('mongodb.uri');

const connectMongoose = (): void => {
  try {
    void mongoose.connect(mongoUri);
  } catch (error) {
    if (error instanceof Error)
      logger.error('Error connecting to MongoDB.', error.message);
    process.exit(-1);
  }
};

mongoose.connection.on('connected', () => {
  logger.info(`Connected to MongoDB at ${mongoose.connection.host}`);
});

mongoose.connection.on('disconnected', () => {
  logger.info(`Disconnected from MongoDB at ${mongoose.connection.host}`);
});

mongoose.connection.on('reconnected', () => {
  logger.info(`Reconnected to MongoDB at ${mongoose.connection.host}`);
});

mongoose.connection.on('error', (error) =>
  logger.error('Mongoose error.', error),
);

export default connectMongoose;
