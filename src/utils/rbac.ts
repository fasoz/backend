export enum Role {
  Guest = 'Guest',
  Researcher = 'Researcher',
  Editor = 'Editor',
  Administrator = 'Administrator',
}

export enum Permission {
  CasesListPublished = 'CasesListPublished', // List published cases
  CasesListUnpublished = 'CasesListUnpublished', // List unpublished cases
  CaseCreate = 'CaseCreate', // Create unpublished cases
  CaseFilesList = 'CaseFilesList', // List case files
  CaseFilesDownload = 'CaseFilesDownload', // Download case files
  CaseReadAnyPublished = 'CaseReadAnyPublished', // Read any published cases
  CaseReadAnyUnpublished = 'CaseReadAnyUnpublished', // Read any unpublished cases
  CaseReadOwnUnpublished = 'CaseReadOwnUnpublished', // Read own unpublished cases
  CaseUpdateAny = 'CaseUpdateAny', // Update any cases
  CaseUpdateOwn = 'CaseUpdateOwn', // Update own cases
  CaseDeleteAny = 'CaseDeleteAny', // Delete any cases
  CaseDeleteOwn = 'CaseDeleteOwn', // Delete own cases
  CasePublishAny = 'CasePublishAny', // Publish any cases
  CasePublishOwn = 'CasePublishOwn', // Publish own cases
  CaseUnpublishAny = 'CaseUnpublishAny', // Unpublish any cases
  CaseUnpublishOwn = 'CaseUnpublishOwn', // Unpublish own cases
  UserList = 'UserList', // List user accounts
  UserCreate = 'UserCreate', // Create user account
  UserReadBasic = 'UserReadBasic', // Read any user's basic information
  UserReadFull = 'UserReadFull', // Read any user's full information
  UserUpdateAny = 'UserUpdateAny', // Update any user account
  UserUpdateSelf = 'UserUpdateSelf', // Update own user account
  UserDeleteAny = 'UserDeleteAny', // Delete any user account
  UserDeleteSelf = 'UserDeleteSelf', // Delete own user account
  UserRolesUpdate = 'UserRolesUpdate', // Assign/Unassign role(s) to any user account
}

/**
 * Returns an array permissions for a certain user role.
 * @param role
 */
const getPermissionsForRole = (role: Role): Array<Permission> => {
  if (role === Role.Administrator)
    return getPermissionsForRole(Role.Editor).concat([
      Permission.UserList,
      Permission.UserUpdateAny,
      Permission.UserDeleteAny,
      Permission.UserRolesUpdate,
    ]);

  if (role === Role.Editor) {
    return getPermissionsForRole(Role.Researcher).concat([
      Permission.CasesListUnpublished,
      Permission.CaseReadAnyUnpublished,
      Permission.CaseUpdateAny,
      Permission.CaseDeleteAny,
      Permission.CasePublishAny,
      Permission.CasePublishOwn,
      Permission.CaseUnpublishAny,
      Permission.UserReadFull,
    ]);
  }

  if (role === Role.Researcher)
    return getPermissionsForRole(Role.Guest).concat([
      Permission.CaseCreate,
      Permission.CaseReadOwnUnpublished,
      Permission.CaseUpdateOwn,
      Permission.CaseDeleteOwn,
      Permission.CaseUnpublishOwn,
    ]);

  if (role === Role.Guest)
    return [
      Permission.CaseFilesList,
      Permission.CaseFilesDownload,
      Permission.CasesListPublished,
      Permission.CaseReadAnyPublished,
      Permission.UserCreate,
      Permission.UserReadBasic,
      Permission.UserUpdateSelf,
      Permission.UserDeleteSelf,
    ];

  return [];
};

/**
 * True if at least one of the given roles has a certain permission.
 * @param roles
 * @param permission
 */
const hasPermission = (roles: Array<Role>, permission: Permission): boolean => {
  for (const role of roles) {
    if (getPermissionsForRole(role).find((p) => p === permission)) {
      return true;
    }
  }
  return false;
};

export default {
  getPermissionsForRole,
  hasPermission,
};
