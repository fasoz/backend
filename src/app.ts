import config from 'config';
import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import morgan from 'morgan';

require('express-async-errors');
import connectMongoose from './utils/mongoose';

import {
  accessTokenExtrator,
  permissionsExtractor,
} from './middlewares/permissions';
import { errorHandler } from './middlewares/errorHandler';
import { unknownEndpoint } from './middlewares/unknownEndpoint';

import adminRouter from './routes/admin';
import healthRouter from './routes/health';
import authRouter from './routes/auth';
import usersRouter from './routes/users';
import casesRouter from './routes/cases';
import caseFilesRouter from './routes/caseFiles';
import caseSearchRouter from './routes/caseSearch';
import contactRouter from './routes/contact';
import testRouter from './routes/test';

// connect to MongoDB using Mongoose
void connectMongoose();

// set up express, its middlewares and endpoints
const app = express();

const corsOptions: cors.CorsOptions = config.get('cors');
app.use(cors(corsOptions));

app.use(helmet());

app.use(express.json());

const morganFormat: string = config.get('logging.morganFormat');
if (morganFormat) app.use(morgan(morganFormat));

app.use(accessTokenExtrator);
app.use(permissionsExtractor);

app.use('/api/admin', adminRouter);
app.use('/api/case/search', caseSearchRouter);
app.use('/api/case', caseFilesRouter);
app.use('/api/case', casesRouter);
app.use('/api/contact', contactRouter);
app.use('/api/health', healthRouter);
app.use('/api/user', usersRouter);
app.use('/api/auth', authRouter);

if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'test') {
  app.use('/api/testing', testRouter);
}

app.use(errorHandler);
app.use(unknownEndpoint);

export default app;
