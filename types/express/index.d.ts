declare namespace Express {
  interface Request {
    accessToken: import('../../src/types').AccessTokenPayload | null;
    userPermissions: Array<import('../../src/utils/rbac').Permission>;
  }
}
