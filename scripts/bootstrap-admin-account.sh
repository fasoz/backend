#!/bin/bash
# calls endpoint for creating an admin user account
# only works in dev and test environments
USERNAME=admin@example.com
PASSWORD=admin123456789

set -e
curl -X 'POST' --fail -H 'Content-Type: application/json' --data "{\"username\":\"${USERNAME}\", \"password\":\"${PASSWORD}\"}" 'http://localhost:3001/api/testing/create_admin'
echo "username: ${USERNAME}"
echo "password: ${PASSWORD}"
