#!/bin/bash
# When making changes to this file, please make sure you also do the corresponding
# modifications to the file `start-docker-containers-with-dev-infrastructure.sh`

podman pod create --name fallarchiv --replace -p 5601:5601 -p 8081:8081 -p 9000:9000 -p 9200:9200 -p 9300:9300 -p 27017:27017
podman run -d --pod fallarchiv --name fallarchiv-minio --rm -e "MINIO_ACCESS_KEY=QWERTYUIOPASDFGHJKLZ" -e "MINIO_SECRET_KEY=qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDF" minio/minio:RELEASE.2021-02-14T04-01-33Z server /data
podman run -d --pod fallarchiv --name fallarchiv-mongodb --rm docker.io/library/mongo:4.4
podman run -d --pod fallarchiv --name fallarchiv-mongo-express --rm -e ME_CONFIG_MONGODB_SERVER=localhost docker.io/library/mongo-express:latest
podman run -d --pod fallarchiv --name fallarchiv-elasticsearch --rm -e "discovery.type=single-node" -e "bootstrap.memory_lock=true" -e "ES_JAVA_OPTS=-Xms1g -Xmx1g" docker.elastic.co/elasticsearch/elasticsearch:7.14.1
podman run -d --pod fallarchiv --name fallarchiv-kibana --rm -e "ELASTICSEARCH_URL=http://localhost:9200" -e "ELASTICSEARCH_HOSTS=http://localhost:9200" docker.elastic.co/kibana/kibana:7.14.1


echo "Open http://localhost:8081 to access Mongo Express."
echo "Open http://localhost:5601 to access Kibana."
echo "Run 'podman logs -nf fallarchiv-minio fallarchiv-mongodb fallarchiv-elasticsearch' to follow the container logs."
echo "Run 'podman pod stop fallarchiv' to stop everything again."
