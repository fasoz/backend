#!/bin/bash
# When making changes to this file, please make sure you also do the corresponding
# modifications to the file `start-podman-pod-with-dev-infrastructure.sh`.
# Note that this script is simplified compared to the podman script.

docker run -d --name fallarchiv-minio --rm -p 9000:9000 -e "MINIO_ACCESS_KEY=QWERTYUIOPASDFGHJKLZ" -e "MINIO_SECRET_KEY=qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDF" minio/minio:RELEASE.2021-02-14T04-01-33Z server /data
docker run -d --name fallarchiv-mongodb --rm -p 27017:27017 docker.io/library/mongo:4.4
docker run -d --name fallarchiv-elasticsearch --rm -p 9100:9100 -p 9200:9200 -e "discovery.type=single-node" -e "bootstrap.memory_lock=true" -e "ES_JAVA_OPTS=-Xms1g -Xmx1g" docker.elastic.co/elasticsearch/elasticsearch:7.14.1


echo "Run 'docker logs -nf fallarchiv-minio fallarchiv-mongodb fallarchiv-elasticsearch' to follow the container logs."
echo "Run 'docker stop -i fallarchiv-minio fallarchiv-mongodb fallarchiv-elasticsearch' to stop everything again."
