/**
 * Tests for /api/case endpoints.
 * Test that are primarily about permissions belong in `case_api_rbac.test.ts`.
 */

import helper from './test_helper';
import supertest from 'supertest';
import mongoose from 'mongoose';
import fs from 'fs';
import app from '../src/app';
import Case from '../src/models/case';
import caseFileStorage from '../src/services/caseFileStorage';
import caseSearch from '../src/services/caseSearch';
import { CaseFile, FulltextSearchResult } from '../src/types';
import { Role } from '../src/utils/rbac';

const api = supertest(app);

beforeAll(async () => {
  await caseSearch.ensureIndexExists();
});

beforeEach(async () => {
  await Promise.all([
    caseFileStorage.deleteAllCases(),
    caseSearch.clearIndex(),
  ]);
  await helper.prepareCaseFixtures();
});

describe('without logging in', () => {
  test('cases fixtures are returned as json', async () => {
    await api
      .get('/api/case')
      .expect(200)
      .expect('Content-Type', /application\/json/);
  });

  test('cases can be queried by id', async () => {
    const caseId = await helper.getValidCaseId();
    const response = await api
      .get(`/api/case/${caseId}`)
      .expect(200)
      .expect('Content-Type', /application\/json/);
    expect(response.body.title).toBeDefined();
    expect(response.body.description).toBeDefined();
    expect(response.body.fileCategories).toBeDefined();
    expect(response.body.keywords).toBeDefined();
    expect(response.body.owners).toBeDefined();
    expect(response.body.participants).toBeDefined();
    expect(response.body.problemAreas).toBeDefined();
    expect(response.body.pedagogicFields).toBeDefined();
    expect(response.body.published).toBeTruthy();
  });

  test('case listing only returns published cases', async () => {
    const response = await api
      .get('/api/case')
      .expect(200)
      .expect('Content-Type', /application\/json/);

    expect(response.body).toHaveLength(
      helper.initialCases.filter((thisCase) => thisCase.published).length,
    );

    for (const c of response.body) expect(c.published).toEqual(true);
  });

  test('a list of file categories can be obtained', async () => {
    const response = await api
      .get('/api/case/file_categories')
      .expect(200)
      .expect('Content-Type', /application\/json/);

    const expected = [
      ...new Set(
        helper.initialCases
          .filter((c) => c.published)
          .map((c) => c.fileCategories)
          .reduce(
            (acc: Array<string>, fileCategories) =>
              acc.concat(fileCategories || []),
            [],
          )
          .sort(),
      ),
    ];

    expect(response.body).toEqual(expected);
  });

  test('a list of keywords can be obtained', async () => {
    const response = await api
      .get('/api/case/keywords')
      .expect(200)
      .expect('Content-Type', /application\/json/);

    const expected = [
      ...new Set(
        helper.initialCases
          .filter((c) => c.published)
          .map((c) => c.keywords)
          .reduce(
            (acc: Array<string>, keywords) => acc.concat(keywords || []),
            [],
          )
          .sort(),
      ),
    ];

    expect(response.body).toEqual(expected);
  });

  test('a list of problem areas can be obtained', async () => {
    const response = await api
      .get('/api/case/problem_areas')
      .expect(200)
      .expect('Content-Type', /application\/json/);

    const expected = [
      ...new Set(
        helper.initialCases
          .filter((c) => c.published)
          .map((c) => c.problemAreas)
          .reduce(
            (acc: Array<string>, problemAreas) =>
              acc.concat(problemAreas || []),
            [],
          )
          .sort(),
      ),
    ];

    expect(response.body).toEqual(expected);
  });

  test('a list of pedagogic fields can be obtained', async () => {
    const response = await api
      .get('/api/case/pedagogic_fields')
      .expect(200)
      .expect('Content-Type', /application\/json/);

    const expected = [
      ...new Set(
        helper.initialCases
          .filter((c) => c.published)
          .map((c) => c.pedagogicFields)
          .reduce(
            (acc: Array<string>, pedagogicFields) =>
              acc.concat(pedagogicFields || []),
            [],
          )
          .sort(),
      ),
    ];

    expect(response.body).toEqual(expected);
  });

  test('fetching a case with an invalid ID returns 404 not found', async () => {
    await api.get('/api/case/iMadeThisUp').expect(404);
  });

  test('listing case files is not allowed', async () => {
    const caseId = await helper.getValidCaseId();
    await api
      .get(`/api/case/${caseId}/file`)
      .expect(401)
      .expect('Content-Type', /application\/json/);
  });

  test('downloading case files is not allowed', async () => {
    const caseId = await helper.getValidCaseId();
    await api
      .get(`/api/case/${caseId}/file/shouldntMatter.pdf`)
      .expect(401)
      .expect('Content-Type', /application\/json/);
  });

  test('creating a new case fails', async () => {
    const newCase = {
      title: 'Lorem Ipsum',
    };
    const response = await api
      .post('/api/case/')
      .send(newCase)
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(/token missing/i);
  });

  test('updating a case fails', async () => {
    const caseId = await helper.getValidCaseId();
    const response = await api
      .put(`/api/case/${caseId}`)
      .send({ title: 'Lorem Ipsum' })
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(/token missing/i);
  });

  test('deleting a case fails', async () => {
    const caseId = await helper.getValidCaseId();
    const response = await api
      .delete(`/api/case/${caseId}`)
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(/token missing/i);
  });

  test('uploading a file to a case fails', async () => {
    const caseId = await helper.getValidCaseId();
    const response = await api
      .post(`/api/case/${caseId}/file`)
      .attach('file', `${process.cwd()}/tests/fixtures/example.pdf`)
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(/token missing/i);
  });

  test('deleting a file from a case fails', async () => {
    const caseFile = await helper.getValidCaseFile();
    const caseFilesAtTheStart = (
      await api.get(`/api/case/${caseFile.caseId}/file`)
    ).body as Array<CaseFile>;

    const response = await api
      .delete(`/api/case/${caseFile.caseId}/file/${caseFile.path}`)
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(/token missing/i);

    const caseFilesAtTheEnd = (
      await api.get(`/api/case/${caseFile.caseId}/file`)
    ).body as Array<CaseFile>;
    expect(caseFilesAtTheStart).toEqual(caseFilesAtTheEnd);
  });

  describe('fulltext search', () => {
    test('for term "Jacky" returns the case of the same name', async () => {
      const response = await api
        .post('/api/case/search/simple')
        .send({ q: 'Jacky' })
        .expect(200)
        .expect('Content-Type', /application\/json/);
      expect(response.body.hits).toHaveLength(1);
      expect(
        response.body.hits.map((c: FulltextSearchResult) => c.case.title),
      ).toContain('Jacky');
    });

    test('for term "ipsum dolor" returns both case fixtures', async () => {
      const response = await api
        .post('/api/case/search/simple')
        .send({ q: 'ipsum dolor' })
        .expect(200)
        .expect('Content-Type', /application\/json/);
      const titles = response.body.hits.map(
        (c: FulltextSearchResult) => c.case.title,
      );
      expect(response.body.hits).toHaveLength(2);
      expect(titles).toContain('Jacky');
      expect(titles).toContain('Kevin');
    });

    test('for something entirely unrelated returns nothing', async () => {
      const response = await api
        .post('/api/case/search/simple')
        .send({ q: 'Tofusandwichpreisentwicklung' })
        .expect(200);
      expect(response.body.numHits.value).toEqual(0);
      expect(response.body.hits).toEqual([]);
    });

    test('results do not include unpublished cases', async () => {
      const response = await api
        .post('/api/case/search/simple')
        .send({ q: 'Incomplete draft' })
        .expect(200);

      response.body.hits.map((result: FulltextSearchResult) => {
        expect(result.case.published).toBeTruthy();
      });
    });
  });
});

describe.each([Role.Administrator, Role.Editor])(
  'when logged in with role "%s"',
  (role) => {
    let accessToken: string;
    let myUserId: string;

    beforeEach(async () => {
      await helper.prepareUserFixtures();
      const tokens = await helper.getValidTokens([role]);
      accessToken = tokens.accessToken;
      myUserId = tokens.userId;
    });

    test('case listing returns published and unpublished cases', async () => {
      const response = await api
        .get('/api/case')
        .set('Authorization', `bearer ${accessToken}`)
        .expect(200)
        .expect('Content-Type', /application\/json/);

      expect(response.body).toHaveLength(helper.initialCases.length);
    });

    describe('creating a new case', () => {
      test('succeeds with just required fields', async () => {
        const casesAtTheStart = await helper.casesInDb();
        const newCase = {
          title: 'Lorem Ipsum',
        };

        const response = await api
          .post('/api/case')
          .set('Authorization', `bearer ${accessToken}`)
          .send(newCase)
          .expect(201)
          .expect('Content-Type', /application\/json/);
        expect(response.body.fileCategories).toEqual([]);
        expect(response.body.keywords).toEqual([]);
        expect(response.body.participants).toEqual('');
        expect(response.body.pedagogicFields).toEqual([]);
        expect(response.body.problemAreas).toEqual([]);
        expect(response.body.published).toEqual(false);

        const casesAtTheEnd = await helper.casesInDb();
        expect(casesAtTheEnd.length).toEqual(casesAtTheStart.length + 1);
      });

      test('succeeds with all fields', async () => {
        const casesAtTheStart = await helper.casesInDb();
        const newCase = {
          title: 'Lorem Ipsum',
          description: 'Donec vitae sapien ut libero venenatis faucibus',
          fileCategories: ['Biografie', 'Interview', 'Genogramm'],
          keywords: ['eins', 'zwei', 'drei'],
          participants: 'Kane, Darth Vader',
          pedagogicFields: ['Libero'],
          problemAreas: ['Vitae', 'Sapien'],
          published: true,
        };

        const response = await api
          .post('/api/case')
          .set('Authorization', `bearer ${accessToken}`)
          .send(newCase)
          .expect(201)
          .expect('Content-Type', /application\/json/);
        expect(response.body).toMatchObject(newCase);

        const casesAtTheEnd = await helper.casesInDb();
        expect(casesAtTheEnd.length).toEqual(casesAtTheStart.length + 1);
      });

      test('without a description defaults to empty string', async () => {
        const newCase = {
          title: 'This interesting',
        };

        const response = await api
          .post('/api/case')
          .set('Authorization', `bearer ${accessToken}`)
          .send(newCase)
          .expect(201)
          .expect('Content-Type', /application\/json/);

        expect(response.body.description).toBeDefined();
        expect(response.body.description).toEqual('');
      });

      test('succeeds with an empty description', async () => {
        const newCase = {
          title: 'This interesting',
          description: '',
        };

        const response = await api
          .post('/api/case')
          .set('Authorization', `bearer ${accessToken}`)
          .send(newCase)
          .expect(201)
          .expect('Content-Type', /application\/json/);

        expect(response.body.description).toEqual('');
      });

      test('fails without required field', async () => {
        const casesAtTheStart = await helper.casesInDb();
        const newCase = {
          description: 'Donec vitae sapien ut libero venenatis faucibus',
          keywords: ['eins', 'zwei', 'drei'],
          participants: 'Kane, Darth Vader',
        };

        const response = await api
          .post('/api/case')
          .set('Authorization', `bearer ${accessToken}`)
          .send(newCase)
          .expect(400)
          .expect('Content-Type', /application\/json/);
        expect(response.body.error).toMatch(/is a required field/i);

        const casesAtTheEnd = await helper.casesInDb();
        expect(casesAtTheEnd.length).toEqual(casesAtTheStart.length);
      });

      test('fails if title already exists', async () => {
        const casesAtTheStart = await helper.casesInDb();
        const newCase = {
          title: helper.initialCases[0].title,
        };

        const response = await api
          .post('/api/case')
          .set('Authorization', `bearer ${accessToken}`)
          .send(newCase)
          .expect(400)
          .expect('Content-Type', /application\/json/);
        expect(response.body.error).toMatch(/duplicate key/i);

        const casesAtTheEnd = await helper.casesInDb();
        expect(casesAtTheEnd.length).toEqual(casesAtTheStart.length);
      });

      test('associates the case with the user who created it', async () => {
        const response = await api
          .post('/api/case')
          .set('Authorization', `bearer ${accessToken}`)
          .send({
            title: 'Lorem Ipsum',
          })
          .expect(201)
          .expect('Content-Type', /application\/json/);
        expect(response.body.owners).toEqual([myUserId]);
      });

      test('associates the user account with their created case', async () => {
        const caseResponse = await api
          .post('/api/case')
          .set('Authorization', `bearer ${accessToken}`)
          .send({
            title: 'Lorem Ipsum',
          })
          .expect(201)
          .expect('Content-Type', /application\/json/);

        const userResponse = await api
          .get(`/api/user/${myUserId}`)
          .set('Authorization', `bearer ${accessToken}`)
          .expect(200)
          .expect('Content-Type', /application\/json/);
        expect(userResponse.body.cases).toHaveLength(1);
        expect(userResponse.body.cases[0].id).toEqual(caseResponse.body.id);
      });
    });

    describe('updating a case', () => {
      test('with a valid id succeeds', async () => {
        const caseId = await helper.getValidCaseId();
        const updatedCase = {
          title: 'Lorem Ipsum',
          description: 'Donec vitae sapien ut libero venenatis faucibus',
          fileCategories: ['Flop', 'Turn', 'River'],
          keywords: ['eins', 'zwei', 'drei'],
          participants: 'Kane, Darth Vader',
          pedagogicFields: ['Libero'],
          problemAreas: ['Vitae', 'Sapien'],
          published: true,
        };
        const response = await api
          .put(`/api/case/${caseId}`)
          .set('Authorization', `bearer ${accessToken}`)
          .send(updatedCase)
          .expect(200)
          .expect('Content-Type', /application\/json/);
        expect(response.body).toMatchObject(updatedCase);
      });

      test('with an invalid id fails', async () => {
        const updatedCase = {
          title: 'Lorem Ipsum',
          description: 'Donec vitae sapien ut libero venenatis faucibus',
          fileCategories: ['La', 'Le', 'Lu'],
          keywords: ['eins', 'zwei', 'drei'],
          participants: 'Kane, Darth Vader',
          pedagogicFields: ['Libero'],
          problemAreas: ['Vitae', 'Sapien'],
          published: true,
        };
        const response = await api
          .put('/api/case/thisIdWasMadeUp123')
          .set('Authorization', `bearer ${accessToken}`)
          .send(updatedCase)
          .expect(404)
          .expect('Content-Type', /application\/json/);
        expect(response.body.error).toMatch(/invalid id/i);
      });

      test('publishing an unpublished case succeeds', async () => {
        const newCase = new Case({
          title: 'Expedita Qui Dolorem Tempore',
          published: false,
        });
        const savedCase = await newCase.save();

        const response = await api
          .put(`/api/case/${savedCase._id}`)
          .set('Authorization', `bearer ${accessToken}`)
          .send({
            ...savedCase.toJSON(),
            published: true,
          })
          .expect(200)
          .expect('Content-Type', /application\/json/);
        expect(response.body.published).toBeTruthy();
      });

      test('unpublishing a published case succeeds', async () => {
        const newCase = new Case({
          title: 'Expedita Qui Dolorem Tempore',
          published: true,
        });
        const savedCase = await newCase.save();

        const response = await api
          .put(`/api/case/${savedCase._id}`)
          .set('Authorization', `bearer ${accessToken}`)
          .send({
            ...savedCase.toJSON(),
            published: false,
          })
          .expect(200)
          .expect('Content-Type', /application\/json/);
        expect(response.body.published).toBeFalsy();
      });
    });

    describe('deleting a case', () => {
      test('with a valid id succeeds', async () => {
        const casesAtTheStart = await helper.casesInDb();
        const caseId = await helper.getValidCaseId();
        await api
          .delete(`/api/case/${caseId}`)
          .set('Authorization', `bearer ${accessToken}`)
          .expect(204);
        const casesAtTheEnd = await helper.casesInDb();
        expect(casesAtTheEnd.length).toEqual(casesAtTheStart.length - 1);
      });

      test('with an invalid id fails', async () => {
        const casesAtTheStart = await helper.casesInDb();

        const response = await api
          .delete('/api/case/thisIdWasMadeUp123')
          .set('Authorization', `bearer ${accessToken}`)
          .expect(404)
          .expect('Content-Type', /application\/json/);
        expect(response.body.error).toMatch(/invalid id/i);

        const casesAtTheEnd = await helper.casesInDb();
        expect(casesAtTheEnd.length).toEqual(casesAtTheStart.length);
      });

      test('deleting a case does not leave any files behind', async () => {
        const caseId = await helper.getValidCaseId();

        // upload a file
        await api
          .post(`/api/case/${caseId}/file`)
          .set('Authorization', `bearer ${accessToken}`)
          .attach('file', `${process.cwd()}/tests/fixtures/example.pdf`)
          .expect(200)
          .expect('Content-Type', /application\/json/);

        // delete the case
        await api
          .delete(`/api/case/${caseId}`)
          .set('Authorization', `bearer ${accessToken}`)
          .expect(204);

        // uploaded file should be deleted
        await api
          .get(`/api/case/${caseId}/file/example.pdf`)
          .set('Authorization', `bearer ${accessToken}`)
          .expect(404)
          .expect('Content-Type', /application\/json/);
      });
    });

    describe('uploading a file', () => {
      test('succeeds with a PDF', async () => {
        const caseId = await helper.getValidCaseId();
        const caseFilesAtTheStart = (
          await api
            .get(`/api/case/${caseId}/file`)
            .set('Authorization', `bearer ${accessToken}`)
        ).body as Array<CaseFile>;
        await api
          .post(`/api/case/${caseId}/file`)
          .set('Authorization', `bearer ${accessToken}`)
          .attach('file', `${process.cwd()}/tests/fixtures/example.pdf`)
          .expect(200)
          .expect('Content-Type', /application\/json/);

        const caseFilesAtTheEnd = (
          await api
            .get(`/api/case/${caseId}/file`)
            .set('Authorization', `bearer ${accessToken}`)
        ).body as Array<CaseFile>;
        expect(caseFilesAtTheEnd.length).toEqual(
          caseFilesAtTheStart.length + 1,
        );
        expect(caseFilesAtTheEnd.map((file: CaseFile) => file.path)).toContain(
          'example.pdf',
        );

        await api
          .get(`/api/case/${caseId}/file/example.pdf`)
          .set('Authorization', `bearer ${accessToken}`)
          .expect(200)
          .expect('Content-Type', /application\/pdf/);
      });

      test('to a sub-folder succeeds', async () => {
        const caseId = await helper.getValidCaseId();
        const caseFilesAtTheStart = (
          await api
            .get(`/api/case/${caseId}/file`)
            .set('Authorization', `bearer ${accessToken}`)
        ).body as Array<CaseFile>;

        await api
          .post(`/api/case/${caseId}/file/Primärdaten`)
          .set('Authorization', `bearer ${accessToken}`)
          .attach('file', `${process.cwd()}/tests/fixtures/example.pdf`)
          .expect(200)
          .expect('Content-Type', /application\/json/);

        const caseFilesAtTheEnd = (
          await api
            .get(`/api/case/${caseId}/file`)
            .set('Authorization', `bearer ${accessToken}`)
        ).body as Array<CaseFile>;

        expect(caseFilesAtTheEnd.length).toEqual(
          caseFilesAtTheStart.length + 1,
        );
        expect(caseFilesAtTheEnd.map((file: CaseFile) => file.path)).toContain(
          'Primärdaten/example.pdf',
        );

        await api
          .get(`/api/case/${caseId}/file/Primärdaten/example.pdf`)
          .set('Authorization', `bearer ${accessToken}`)
          .expect(200)
          .expect('Content-Type', /application\/pdf/);
      });

      test('works with two files', async () => {
        const caseId = await helper.getValidCaseId();
        const caseFilesAtTheStart = (
          await api
            .get(`/api/case/${caseId}/file`)
            .set('Authorization', `bearer ${accessToken}`)
        ).body as Array<CaseFile>;

        await api
          .post(`/api/case/${caseId}/file`)
          .set('Authorization', `bearer ${accessToken}`)
          .attach('file1', `${process.cwd()}/tests/fixtures/example.pdf`)
          .attach('file2', `${process.cwd()}/tests/fixtures/emptyFile.txt`)
          .expect(200)
          .expect('Content-Type', /application\/json/);

        const caseFilesAtTheEnd = (
          await api
            .get(`/api/case/${caseId}/file`)
            .set('Authorization', `bearer ${accessToken}`)
        ).body as Array<CaseFile>;

        expect(caseFilesAtTheEnd.length).toEqual(
          caseFilesAtTheStart.length + 2,
        );
        expect(caseFilesAtTheEnd.map((file: CaseFile) => file.path)).toContain(
          'example.pdf',
        );
        expect(caseFilesAtTheEnd.map((file: CaseFile) => file.path)).toContain(
          'emptyFile.txt',
        );
      });
    });

    describe('downloading a file', () => {
      test('works and the downloaded file is identical to the one that was uploaded', async () => {
        const caseId = await helper.getValidCaseId();

        // upload file
        await api
          .post(`/api/case/${caseId}/file`)
          .set('Authorization', `bearer ${accessToken}`)
          .attach('file', `${process.cwd()}/tests/fixtures/example.pdf`)
          .expect(200)
          .expect('Content-Type', /application\/json/);

        // download file
        const response = await api
          .get(`/api/case/${caseId}/file/example.pdf`)
          .set('Authorization', `bearer ${accessToken}`)
          .expect(200)
          .expect('Content-Type', /application\/pdf/);

        // compare file contents byte for byte
        const uploadedFile = fs.readFileSync(
          `${process.cwd()}/tests/fixtures/example.pdf`,
        );
        expect(response.body).toEqual(uploadedFile);
      });

      test('succeed with special ascii characters in the file name', async () => {
        const caseId = await helper.getValidCaseId();

        // upload file
        await api
          .post(`/api/case/${caseId}/file`)
          .set('Authorization', `bearer ${accessToken}`)
          .attach('file', `${process.cwd()}/tests/fixtures/example.pdf`, {
            filename: 'äöüh & !$().pdf',
          })
          .expect(200)
          .expect('Content-Type', /application\/json/);

        // check file listing
        const response = await api
          .get(`/api/case/${caseId}/file`)
          .set('Authorization', `bearer ${accessToken}`)
          .expect(200)
          .expect('Content-Type', /application\/json/);
        expect(response.body.map((f: CaseFile) => f.path)).toContain(
          'äöüh & !$().pdf',
        );

        // download file
        await api
          .get(`/api/case/${caseId}/file/äöüh & !$().pdf`)
          .set('Authorization', `bearer ${accessToken}`)
          .expect(200)
          .expect('Content-Type', /application\/pdf/);
      });

      test("that doesn't exist returns 404 not found", async () => {
        const caseId = await helper.getValidCaseId();
        const response = await api
          .get(`/api/case/${caseId}/file/thisDoesNotExist.pdf`)
          .set('Authorization', `bearer ${accessToken}`)
          .expect(404);
        expect(response.body.error).toMatch(/file not found/i);
      });
    });

    describe('deleting a file from a case', () => {
      test('succeeds with a valid id and filename', async () => {
        const caseFile = await helper.getValidCaseFile();
        const caseFilesAtTheStart = (
          await api
            .get(`/api/case/${caseFile.caseId}/file`)
            .set('Authorization', `bearer ${accessToken}`)
        ).body as Array<CaseFile>;

        await api
          .delete(`/api/case/${caseFile.caseId}/file/${caseFile.path}`)
          .set('Authorization', `bearer ${accessToken}`)
          .expect(204);

        const caseFilesAtTheEnd = (
          await api
            .get(`/api/case/${caseFile.caseId}/file`)
            .set('Authorization', `bearer ${accessToken}`)
        ).body as Array<CaseFile>;

        expect(caseFilesAtTheEnd.length).toEqual(
          caseFilesAtTheStart.length - 1,
        );
      });

      test('fails with an invalid id', async () => {
        const caseFile = await helper.getValidCaseFile();
        await api
          .delete(`/api/case/23h2kj4h6352j/file/${caseFile.path}`)
          .set('Authorization', `bearer ${accessToken}`)
          .expect(404);
      });

      test('fails with an invalid filename', async () => {
        const caseFile = await helper.getValidCaseFile();
        await api
          .delete(`/api/case/${caseFile.caseId}/file/badFilename.pdf`)
          .set('Authorization', `bearer ${accessToken}`)
          .expect(204);
      });
    });
  },
);

describe('when logged in with role "Researcher"', () => {
  let accessToken: string;
  let myUserId: string;

  beforeEach(async () => {
    await helper.prepareUserFixtures();
    const tokens = await helper.getValidTokens([Role.Researcher]);
    accessToken = tokens.accessToken;
    myUserId = tokens.userId;
  });

  test('case listing only returns published cases', async () => {
    const response = await api
      .get('/api/case')
      .set('Authorization', `bearer ${accessToken}`)
      .expect(200)
      .expect('Content-Type', /application\/json/);

    expect(response.body).toHaveLength(
      helper.initialCases.filter((thisCase) => thisCase.published).length,
    );

    for (const c of response.body) expect(c.published).toEqual(true);
  });

  test('creating a case leaves it unpublished', async () => {
    const response = await api
      .post('/api/case')
      .send({ title: 'Lorem ipsum' })
      .set('Authorization', `bearer ${accessToken}`)
      .expect(201)
      .expect('Content-Type', /application\/json/);
    expect(response.body.published).toEqual(false);
  });

  test('creating a case and explicitly setting it to published fails', async () => {
    await api
      .post('/api/case')
      .send({ title: 'Lorem ipsum', published: true })
      .set('Authorization', `bearer ${accessToken}`)
      .expect(403)
      .expect('Content-Type', /application\/json/);
  });

  test('reading your own unpublished case succeeds', async () => {
    const myCase = new Case({ title: 'Lorem Ipsum', owners: [myUserId] });
    await myCase.save();

    await api
      .get(`/api/case/${myCase._id}`)
      .set('Authorization', `bearer ${accessToken}`)
      .expect(200)
      .expect('Content-Type', /application\/json/);
  });

  test('reading someone elses unpublished case fails', async () => {
    const someoneElsesCase = new Case({ title: 'Lorem Ipsum' });
    await someoneElsesCase.save();

    await api
      .get(`/api/case/${someoneElsesCase._id}`)
      .set('Authorization', `bearer ${accessToken}`)
      .expect(403)
      .expect('Content-Type', /application\/json/);
  });

  test("you can't publish a case yourself", async () => {
    const myCase = new Case({ title: 'Lorem Ipsum', owners: [myUserId] });
    await myCase.save();
    expect(myCase.published).toEqual(false);

    await api
      .put(`/api/case/${myCase._id}`)
      .set('Authorization', `bearer ${accessToken}`)
      .send({ title: 'Lorem Ipsum', published: true })
      .expect(403)
      .expect('Content-Type', /application\/json/);
  });

  test('you can unpublish a case yourself', async () => {
    const myCase = new Case({
      title: 'Lorem Ipsum',
      owners: [myUserId],
      published: true,
    });
    await myCase.save();

    await api
      .put(`/api/case/${myCase._id}`)
      .set('Authorization', `bearer ${accessToken}`)
      .send({ title: 'Lorem Ipsum', published: false })
      .expect(200)
      .expect('Content-Type', /application\/json/);
  });

  test('updating your own case succeeds', async () => {
    const myCase = new Case({ title: 'Lorem Ipsum', owners: [myUserId] });
    await myCase.save();

    const response = await api
      .put(`/api/case/${myCase._id}`)
      .set('Authorization', `bearer ${accessToken}`)
      .send({ title: 'Lorem Ipsum', description: 'Dolor sit amet.' })
      .expect(200)
      .expect('Content-Type', /application\/json/);

    expect(response.body.description).toEqual('Dolor sit amet.');
  });

  test('updating someone elses case fails', async () => {
    const someoneElsesCase = new Case({ title: 'Lorem Ipsum' });
    await someoneElsesCase.save();

    await api
      .put(`/api/case/${someoneElsesCase._id}`)
      .set('Authorization', `bearer ${accessToken}`)
      .send({ title: 'Internet Vandalism' })
      .expect(403)
      .expect('Content-Type', /application\/json/);
  });

  test('deleting your own case succeeds', async () => {
    const myCase = new Case({ title: 'Lorem Ipsum', owners: [myUserId] });
    await myCase.save();

    await api
      .delete(`/api/case/${myCase._id}`)
      .set('Authorization', `bearer ${accessToken}`)
      .expect(204);
  });

  test('deleting someone elses case fails', async () => {
    const someoneElsesCase = new Case({ title: 'Lorem Ipsum' });
    await someoneElsesCase.save();

    await api
      .delete(`/api/case/${someoneElsesCase._id}`)
      .set('Authorization', `bearer ${accessToken}`)
      .expect(403)
      .expect('Content-Type', /application\/json/);
  });

  test('uploading files to your own case succeeds', async () => {
    const myCase = new Case({ title: 'Lorem Ipsum', owners: [myUserId] });
    await myCase.save();

    await api
      .post(`/api/case/${myCase._id}/file`)
      .set('Authorization', `bearer ${accessToken}`)
      .attach('file', `${process.cwd()}/tests/fixtures/example.pdf`)
      .expect(200)
      .expect('Content-Type', /application\/json/);
  });

  test('uploading files to someone elses case fails', async () => {
    const someoneElsesCase = new Case({ title: 'Lorem Ipsum' });
    await someoneElsesCase.save();

    await api
      .post(`/api/case/${someoneElsesCase._id}/file`)
      .set('Authorization', `bearer ${accessToken}`)
      .attach('file', `${process.cwd()}/tests/fixtures/example.pdf`)
      .expect(403)
      .expect('Content-Type', /application\/json/);
  });

  test('deleting files from your own case succeeds', async () => {
    const myCase = new Case({ title: 'Lorem Ipsum', owners: [myUserId] });
    await myCase.save();
    if (!myCase._id)
      throw new Error('Saving the CaseDocument did not yield a document ID');

    const fileStream = fs.createReadStream(
      `${process.cwd()}/tests/fixtures/example.pdf`,
    );
    await caseFileStorage.storeFile(myCase._id, 'example.pdf', fileStream);

    await api
      .delete(`/api/case/${myCase._id}/file/example.pdf`)
      .set('Authorization', `bearer ${accessToken}`)
      .expect(204);
  });

  test('deleting files from someone elses case fails', async () => {
    const someoneElsesCase = new Case({ title: 'Lorem Ipsum' });
    await someoneElsesCase.save();
    if (!someoneElsesCase._id)
      throw new Error('Saving the CaseDocument did not yield a document ID');

    const fileStream = fs.createReadStream(
      `${process.cwd()}/tests/fixtures/example.pdf`,
    );
    await caseFileStorage.storeFile(
      someoneElsesCase._id,
      'example.pdf',
      fileStream,
    );

    await api
      .delete(`/api/case/${someoneElsesCase._id}/file/example.pdf`)
      .set('Authorization', `bearer ${accessToken}`)
      .expect(403)
      .expect('Content-Type', /application\/json/);
  });
});

describe('with an invalid token', () => {
  const invalidAccessToken =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM4MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';

  test('creating a new case fails', async () => {
    const casesAtTheStart = await helper.casesInDb();
    const newCase = {
      title: 'Lorem Ipsum',
    };

    const response = await api
      .post('/api/case')
      .set('Authorization', `bearer ${invalidAccessToken}`)
      .send(newCase)
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(/token missing or invalid/i);

    const casesAtTheEnd = await helper.casesInDb();
    expect(casesAtTheEnd.length).toEqual(casesAtTheStart.length);
  });

  test('updating a case fails', async () => {
    const caseId = await helper.getValidCaseId();
    const response = await api
      .put(`/api/case/${caseId}`)
      .set('Authorization', `bearer ${invalidAccessToken}`)
      .send({ title: 'Lorem Ipsum' })
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(/token missing or invalid/i);
  });

  test('deleting a case fails', async () => {
    const caseId = await helper.getValidCaseId();
    const response = await api
      .delete(`/api/case/${caseId}`)
      .set('Authorization', `bearer ${invalidAccessToken}`)
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(/token missing or invalid/i);
  });

  test('uploading a file to a case fails', async () => {
    const caseId = await helper.getValidCaseId();
    const response = await api
      .post(`/api/case/${caseId}/file`)
      .set('Authorization', `bearer ${invalidAccessToken}`)
      .attach('file', `${process.cwd()}/tests/fixtures/example.pdf`)
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(/token missing or invalid/i);
  });

  test('deleting a file from a case fails', async () => {
    const caseFile = await helper.getValidCaseFile();
    const response = await api
      .delete(`/api/case/${caseFile.caseId}/file/badFilename.pdf`)
      .set('Authorization', `bearer ${invalidAccessToken}`)
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(/token missing or invalid/i);
  });
});

describe.each([Role.Administrator, Role.Editor, Role.Researcher, Role.Guest])(
  'file access with role "%s"',
  (userRole) => {
    let accessToken: string;

    beforeEach(async () => {
      await helper.prepareUserFixtures();
      const tokens = await helper.getValidTokens([userRole]);
      accessToken = tokens.accessToken;
    });

    test.each([
      [[Role.Administrator, Role.Editor, Role.Researcher, Role.Guest]],
      [[Role.Administrator, Role.Editor, Role.Researcher]],
      [[Role.Administrator, Role.Editor]],
      [[Role.Administrator]],
      [[Role.Editor, Role.Researcher, Role.Guest]],
      [[Role.Editor, Role.Researcher]],
      [[Role.Editor]],
      [[Role.Researcher]],
      [[Role.Guest]],
      [[]],
    ])('for cases limited to roles "%s"', async (caseRoleRequirement) => {
      const newCase = new Case({
        title: 'Bing Bang Bong Bong Bong',
        published: true,
        fileAccessAllowedRoles: caseRoleRequirement,
      });
      const savedCase = await newCase.save();

      await api
        .get(`/api/case/${savedCase._id}/file`)
        .set('Authorization', `bearer ${accessToken}`)
        .expect(caseRoleRequirement.includes(userRole) ? 200 : 403)
        .expect('Content-Type', /application\/json/);
    });
  },
);

afterAll(() => {
  void mongoose.connection.close();
});
