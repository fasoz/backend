/***
 * RBAC specific tests for /api/user endpoints.
 */

import { Role } from '../src/utils/rbac';
import helper from './test_helper';
import supertest from 'supertest';
import mongoose from 'mongoose';
import User from '../src/models/user';
import app from '../src/app';
import { hashPassword } from '../src/routes/users';

const api = supertest(app);

describe.each([
  null, // no role
  Role.Guest,
  Role.Researcher,
  Role.Editor,
  Role.Administrator,
])('logged in with role "%s"', (role) => {
  let accessToken: string;
  let myUserId: string;

  beforeEach(async () => {
    await helper.prepareUserFixtures();

    const tokens = await helper.getValidTokens(role ? [role] : []);
    accessToken = tokens.accessToken;
    myUserId = tokens.userId;
  });

  test('list all users', async () => {
    await api
      .get('/api/user')
      .set('Authorization', `bearer ${accessToken}`)
      .expect(role === Role.Administrator ? 200 : 403)
      .expect('Content-Type', /application\/json/);
  });

  test('create user', async () => {
    await api
      .post('/api/user')
      .send({ username: 'michael@example.com', password: 'sevenTitles91' })
      .set('Authorization', `bearer ${accessToken}`)
      .expect(201)
      .expect('Content-Type', /application\/json/);
  });

  describe('own user', () => {
    test('read', async () => {
      await api
        .get(`/api/user/${myUserId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .expect(200)
        .expect('Content-Type', /application\/json/);
    });

    test('update', async () => {
      await api
        .put(`/api/user/${myUserId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .send({
          name: 'New Name',
          username: 'new@example.com',
          password: 'neuIstImmerGut',
        })
        .expect(role ? 200 : 403)
        .expect('Content-Type', /application\/json/);
    });

    test('delete', async () => {
      await api
        .delete(`/api/user/${myUserId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .expect(role ? 204 : 403);
    });
  });

  describe('another user', () => {
    let anotherUserId: string;

    beforeEach(async () => {
      const anotherUser = new User({
        emailVerified: true,
        username: 'jean@example.com',
        name: 'Jean Todt',
        passwordHash: hashPassword('IlCavalloRosso'),
      });
      await anotherUser.save();
      anotherUserId = anotherUser._id || '';
    });

    test('read', async () => {
      await api
        .get(`/api/user/${anotherUserId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .expect(role ? 200 : 403)
        .expect('Content-Type', /application\/json/);
    });

    test('update', async () => {
      await api
        .put(`/api/user/${anotherUserId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .send({
          name: 'New Name',
          username: 'new@example.com',
          password: 'neuIstImmerGut',
        })
        .expect(role === Role.Administrator ? 200 : 403)
        .expect('Content-Type', /application\/json/);
    });

    test('delete', async () => {
      await api
        .delete(`/api/user/${anotherUserId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .expect(role === Role.Administrator ? 204 : 403);
    });
  });
});

afterAll(() => {
  void mongoose.connection.close();
});
