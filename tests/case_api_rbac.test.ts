/***
 * RBAC specific tests for /api/case endpoints.
 */

import { Role } from '../src/utils/rbac';
import helper from './test_helper';
import supertest from 'supertest';
import mongoose from 'mongoose';
import app from '../src/app';
import Case from '../src/models/case';
import caseFileStorage from '../src/services/caseFileStorage';
import caseSearch from '../src/services/caseSearch';

const api = supertest(app);

beforeAll(async () => {
  await caseSearch.ensureIndexExists();
});

describe.each([
  null, // no role
  Role.Guest,
  Role.Researcher,
  Role.Editor,
  Role.Administrator,
])('logged in with role "%s"', (role) => {
  let accessToken: string;
  let myUserId: string;

  beforeEach(async () => {
    await Promise.all([
      caseFileStorage.deleteAllCases(),
      caseSearch.clearIndex(),
    ]);
    await Promise.all([
      helper.prepareUserFixtures(),
      helper.prepareCaseFixtures(),
    ]);

    const tokens = await helper.getValidTokens(role ? [role] : []);
    accessToken = tokens.accessToken;
    myUserId = tokens.userId;
  });

  test('list cases', async () => {
    const response = await api
      .get('/api/case')
      .set('Authorization', `bearer ${accessToken}`)
      .expect(200)
      .expect('Content-Type', /application\/json/);

    if (role && [Role.Administrator, Role.Editor].includes(role)) {
      // may see unpublished cases
      expect(response.body).toHaveLength(helper.initialCases.length);
    } else {
      // mau only see published cases
      const numPublishedCases = helper.initialCases.filter(
        (c) => c.published,
      ).length;
      expect(response.body).toHaveLength(numPublishedCases);
    }
  });

  test('create unpublished case', async () => {
    const newCase = {
      title: 'Lorem Ipsum',
      published: false,
    };

    const response = await api
      .post('/api/case')
      .set('Authorization', `bearer ${accessToken}`)
      .send(newCase)
      .expect('Content-Type', /application\/json/);

    if (
      role &&
      [Role.Administrator, Role.Editor, Role.Researcher].includes(role)
    ) {
      expect(response.status).toEqual(201);
    } else {
      expect(response.status).toEqual(403);
    }
  });

  test('create published case', async () => {
    const newCase = {
      title: 'Ipsum Lorem',
      published: true,
    };

    const response = await api
      .post('/api/case')
      .set('Authorization', `bearer ${accessToken}`)
      .send(newCase)
      .expect('Content-Type', /application\/json/);

    if (role && [Role.Administrator, Role.Editor].includes(role)) {
      expect(response.status).toEqual(201);
    } else {
      expect(response.status).toEqual(403);
    }
  });

  describe.each([true, false])('own case (published: %p)', (published) => {
    let myCaseId: string;

    beforeEach(async () => {
      const newCase = new Case({
        title: 'Old title',
        published,
        owners: [myUserId],
      });
      await newCase.save();
      myCaseId = newCase._id || '';
    });

    test('read', async () => {
      const response = await api
        .get(`/api/case/${myCaseId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .expect('Content-Type', /application\/json/);

      if (!published && (!role || role === Role.Guest)) {
        expect(response.status).toEqual(403);
      } else {
        expect(response.status).toEqual(200);
      }
    });

    test('update', async () => {
      const response = await api
        .put(`/api/case/${myCaseId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .send({ title: 'New title' })
        .expect('Content-Type', /application\/json/);

      if (
        role &&
        [Role.Administrator, Role.Editor, Role.Researcher].includes(role)
      ) {
        expect(response.status).toEqual(200);
        expect(response.body.title).toBe('New title');
      } else {
        expect(response.status).toEqual(403);
      }
    });

    test('delete', async () => {
      const response = await api
        .delete(`/api/case/${myCaseId}`)
        .set('Authorization', `bearer ${accessToken}`);

      if (
        role &&
        [Role.Administrator, Role.Editor, Role.Researcher].includes(role)
      ) {
        expect(response.status).toEqual(204);
      } else {
        expect(response.status).toEqual(403);
      }
    });

    test('publish', async () => {
      if (published) {
        // skip test if 'published' is already true
        return;
      }

      const response = await api
        .put(`/api/case/${myCaseId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .send({ title: 'Yet another title', published: true })
        .expect('Content-Type', /application\/json/);

      if (role && [Role.Administrator, Role.Editor].includes(role)) {
        expect(response.status).toEqual(200);
      } else {
        expect(response.status).toEqual(403);
      }
    });

    test('unpublish', async () => {
      const response = await api
        .put(`/api/case/${myCaseId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .send({ title: 'Old title', published: false })
        .expect('Content-Type', /application\/json/);

      if (!role || role == Role.Guest) {
        expect(response.status).toEqual(403);
      } else {
        expect(response.status).toEqual(200);
      }
    });

    test('list files', async () => {
      await api
        .get(`/api/case/${myCaseId}/file`)
        .set('Authorization', `bearer ${accessToken}`)
        .expect(role ? 200 : 403)
        .expect('Content-Type', /application\/json/);
    });

    test('download file', async () => {
      await api
        .get(`/api/case/${myCaseId}/file/gibtsNicht.pdf`)
        .set('Authorization', `bearer ${accessToken}`)
        .expect(role ? 404 : 403)
        .expect('Content-Type', /application\/json/);
    });

    test('upload file', async () => {
      const response = await api
        .post(`/api/case/${myCaseId}/file`)
        .set('Authorization', `bearer ${accessToken}`)
        .attach('file', `${process.cwd()}/tests/fixtures/example.pdf`)
        .expect('Content-Type', /application\/json/);

      if (role && role !== Role.Guest) {
        expect(response.status).toEqual(200);
      } else {
        expect(response.status).toEqual(403);
      }
    });

    test('delete file', async () => {
      const response = await api
        .delete(`/api/case/${myCaseId}/file/gibtsNicht.pdf`)
        .set('Authorization', `bearer ${accessToken}`);

      if (role && role !== Role.Guest) {
        expect(response.status).toEqual(204);
      } else {
        expect(response.status).toEqual(403);
      }
    });
  });

  describe.each([true, false])(
    'someone elses case (published: %p)',
    (published) => {
      let someoneElsesCaseId: string;

      beforeEach(async () => {
        const newCase = new Case({
          title: 'Old title',
          published,
          owners: [],
        });
        await newCase.save();
        someoneElsesCaseId = newCase._id || '';
      });

      test('read', async () => {
        const response = await api
          .get(`/api/case/${someoneElsesCaseId}`)
          .set('Authorization', `bearer ${accessToken}`)
          .expect('Content-Type', /application\/json/);

        if (published) {
          expect(response.status).toEqual(200);
        } else if (role && [Role.Administrator, Role.Editor].includes(role)) {
          expect(response.status).toEqual(200);
        } else {
          expect(response.status).toEqual(403);
        }
      });

      test('update', async () => {
        const response = await api
          .put(`/api/case/${someoneElsesCaseId}`)
          .set('Authorization', `bearer ${accessToken}`)
          .send({ title: 'New title' })
          .expect('Content-Type', /application\/json/);

        if (role && [Role.Administrator, Role.Editor].includes(role)) {
          expect(response.status).toEqual(200);
        } else {
          expect(response.status).toEqual(403);
        }
      });

      test('delete', async () => {
        const response = await api
          .delete(`/api/case/${someoneElsesCaseId}`)
          .set('Authorization', `bearer ${accessToken}`);

        if (role && [Role.Administrator, Role.Editor].includes(role)) {
          expect(response.status).toEqual(204);
        } else {
          expect(response.status).toEqual(403);
        }
      });

      test('publish', async () => {
        const newCase = new Case({
          title: 'Yet another title',
          published: false,
          owners: [],
        });
        await newCase.save();

        const response = await api
          .put(`/api/case/${newCase.toJSON().id}`)
          .set('Authorization', `bearer ${accessToken}`)
          .send({ title: 'Yet another title', published: true })
          .expect('Content-Type', /application\/json/);

        if (role && [Role.Administrator, Role.Editor].includes(role)) {
          expect(response.status).toEqual(200);
        } else {
          expect(response.status).toEqual(403);
        }
      });

      test('unpublish', async () => {
        const response = await api
          .put(`/api/case/${someoneElsesCaseId}`)
          .set('Authorization', `bearer ${accessToken}`)
          .send({ title: 'Old title', published: false })
          .expect('Content-Type', /application\/json/);

        if (role && [Role.Administrator, Role.Editor].includes(role)) {
          expect(response.status).toEqual(200);
        } else {
          expect(response.status).toEqual(403);
        }
      });

      test('list files', async () => {
        await api
          .get(`/api/case/${someoneElsesCaseId}/file`)
          .set('Authorization', `bearer ${accessToken}`)
          .expect(role ? 200 : 403)
          .expect('Content-Type', /application\/json/);
      });

      test('download file', async () => {
        // testing with a file that doesn't actually exist, because this test is about getting 403s in the right cases
        // if the file would exist, the 404s would be 200s
        await api
          .get(`/api/case/${someoneElsesCaseId}/file/gibtsNicht.pdf`)
          .set('Authorization', `bearer ${accessToken}`)
          .expect(role ? 404 : 403)
          .expect('Content-Type', /application\/json/);
      });

      test('upload file', async () => {
        const response = await api
          .post(`/api/case/${someoneElsesCaseId}/file`)
          .set('Authorization', `bearer ${accessToken}`)
          .attach('file', `${process.cwd()}/tests/fixtures/example.pdf`)
          .expect('Content-Type', /application\/json/);

        if (role && [Role.Administrator, Role.Editor].includes(role)) {
          expect(response.status).toEqual(200);
        } else {
          expect(response.status).toEqual(403);
        }
      });

      test('delete file', async () => {
        const response = await api
          .delete(`/api/case/${someoneElsesCaseId}/file/gibtsNicht.pdf`)
          .set('Authorization', `bearer ${accessToken}`);

        if (role && [Role.Administrator, Role.Editor].includes(role)) {
          expect(response.status).toEqual(204);
        } else {
          expect(response.status).toEqual(403);
        }
      });
    },
  );
});

afterAll(() => {
  void mongoose.connection.close();
});
