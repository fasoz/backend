import rbac, { Permission, Role } from '../src/utils/rbac';

/**
 * These tests do not cover all permutations of roles and permissions.
 * Instead we test select samples. Enough to convince ourselves hasPermissions works as it should.
 */

test.each([
  [Role.Administrator, Permission.CasesListPublished, true],
  [Role.Editor, Permission.CasesListPublished, true],
  [Role.Researcher, Permission.CasesListPublished, true],
  [Role.Administrator, Permission.UserRolesUpdate, true],
  [Role.Editor, Permission.UserRolesUpdate, false],
  [Role.Researcher, Permission.UserRolesUpdate, false],
])('hasPermission([%s], %s) is %s', (role, permission, expected) => {
  expect(rbac.hasPermission([role], permission)).toEqual(expected);
});

test('multiple roles, but none has the permission', () => {
  expect(
    rbac.hasPermission(
      [Role.Researcher, Role.Editor],
      Permission.UserRolesUpdate,
    ),
  ).toBeFalsy();
});

test('multiple roles, one has the permission', () => {
  expect(
    rbac.hasPermission(
      [Role.Researcher, Role.Editor, Role.Administrator],
      Permission.UserRolesUpdate,
    ),
  ).toBeTruthy();
});
