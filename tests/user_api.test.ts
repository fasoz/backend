import mongoose from 'mongoose';
import supertest from 'supertest';
import app from '../src/app';
import helper from './test_helper';
import User from '../src/models/user';
import { Role } from '../src/utils/rbac';
import { hashPassword } from '../src/routes/users';

const api = supertest(app);

beforeEach(async () => {
  await helper.prepareUserFixtures();
});

describe(`when logged in with role "${Role.Administrator}"`, () => {
  let accessToken: string;
  let myUserId: string;

  beforeEach(async () => {
    await helper.prepareUserFixtures();
    const tokens = await helper.getValidTokens([Role.Administrator]);
    accessToken = tokens.accessToken;
    myUserId = tokens.userId;
  });

  test('users are returned as json', async () => {
    const response = await api
      .get('/api/user')
      .set('Authorization', `bearer ${accessToken}`)
      .expect(200)
      .expect('Content-Type', /application\/json/);
    expect(response.body).toHaveLength(helper.initialUsers.length);
  });

  test('other users can be queried by id', async () => {
    const userId = await helper.getValidUserId();
    expect(userId).not.toEqual(myUserId);

    const response = await api
      .get(`/api/user/${userId}`)
      .set('Authorization', `bearer ${accessToken}`)
      .expect(200)
      .expect('Content-Type', /application\/json/);

    expect(response.body.cases).toEqual([]);
    expect(response.body.id).toEqual(`${userId}`);
    expect(response.body.roles).toEqual(expect.anything());
    expect(response.body.username).toMatch(/^.+@.+\..+$/);
  });

  test('passwords are not leaked', async () => {
    const response = await api
      .get('/api/user')
      .set('Authorization', `bearer ${accessToken}`);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    response.body.map((user: any) => {
      expect(user.password).toBeUndefined();
    });
  });

  test('creating a user with roles succeeds', async () => {
    const newUser = {
      username: 'alice@example.com',
      name: 'Alice Reed',
      password: '1234567890qwertyui',
      roles: [Role.Administrator, Role.Editor],
    };
    const response = await api
      .post('/api/user')
      .set('Authorization', `bearer ${accessToken}`)
      .send(newUser)
      .expect(201)
      .expect('Content-Type', /application\/json/);
    expect(response.body.roles).toEqual(newUser.roles);
  });

  test('creating a user with invalid roles fails', async () => {
    const newUser = {
      username: 'alice@example.com',
      name: 'Alice Reed',
      password: '1234567890qwertyui',
      roles: ['iMadeThisRoleUp'],
    };
    await api
      .post('/api/user')
      .set('Authorization', `bearer ${accessToken}`)
      .send(newUser)
      .expect(400)
      .expect('Content-Type', /application\/json/);
  });

  test('deleting a user succeeds', async () => {
    const userId = await helper.getValidUserId();
    await api
      .delete(`/api/user/${userId}`)
      .set('Authorization', `bearer ${accessToken}`)
      .expect(204);
  });

  test('editing a user succeeds', async () => {
    const userId = await helper.getValidUserId();
    const editedUser = {
      username: 'alice@example.com',
      name: 'Alice Reed',
      sessions: [],
    };
    const response = await api
      .put(`/api/user/${userId}`)
      .send(editedUser)
      .set('Authorization', `bearer ${accessToken}`)
      .expect(200)
      .expect('Content-Type', /application\/json/);
    expect(response.body).toMatchObject(editedUser);

    const usersAtEnd = await helper.usersInDb();
    expect(usersAtEnd.map((u) => u.name)).toContain('Alice Reed');
    expect(usersAtEnd.map((u) => u.username)).toContain('alice@example.com');
  });

  test('editing a user and leaving the password empty does not modify the password hash', async () => {
    const userId = await helper.getValidUserId();
    const userAtStart = await User.findById(userId);

    const editedUser = {
      username: 'alice@example.com',
      name: 'Alice Reed',
      sessions: [],
    };
    await api
      .put(`/api/user/${userId}`)
      .send(editedUser)
      .set('Authorization', `bearer ${accessToken}`)
      .expect(200)
      .expect('Content-Type', /application\/json/);

    const userAtEnd = await User.findById(userId);
    if (!userAtStart || !userAtEnd) throw new Error('no users in database');

    expect(userAtStart._id).toEqual(userAtEnd._id);
    expect(userAtStart.username).not.toEqual(userAtEnd.username);
    expect(userAtStart.passwordHash).toEqual(userAtEnd.passwordHash);
  });

  test('changing the password works', async () => {
    const userId = await helper.getValidUserId();

    const editedUser = {
      username: 'bobby@example.org',
      name: 'Bobby Tables',
      password: 'iChangedMyPassword23',
      sessions: [],
    };
    await api
      .put(`/api/user/${userId}`)
      .send(editedUser)
      .set('Authorization', `bearer ${accessToken}`)
      .expect(200)
      .expect('Content-Type', /application\/json/);

    await api
      .post(`/api/auth/login`)
      .send({
        username: editedUser.username,
        password: editedUser.password,
      })
      .expect(200);
  });

  test('setting a short password fails', async () => {
    const userId = await helper.getValidUserId();

    const editedUser = {
      username: 'bobby@example.org',
      name: 'Bobby Tables',
      password: 'blubb',
      sessions: [],
    };
    const response = await api
      .put(`/api/user/${userId}`)
      .send(editedUser)
      .set('Authorization', `bearer ${accessToken}`)
      .expect(400)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(
      /password must be at least \d+ characters/i,
    );
  });

  test('editing user roles succeeds', async () => {
    const userId = await helper.getValidUserId();
    const editedUser = {
      username: 'alice@example.com',
      name: 'Alice Reed',
      roles: [Role.Editor, Role.Researcher],
    };
    await api
      .put(`/api/user/${userId}`)
      .send(editedUser)
      .set('Authorization', `bearer ${accessToken}`)
      .expect(200)
      .expect('Content-Type', /application\/json/);
  });
});

describe.each([Role.Editor, Role.Researcher])(
  'when logged in with role "%s"',
  (userRole) => {
    let accessToken: string;
    let myUserId: string;

    beforeEach(async () => {
      await helper.prepareUserFixtures();
      const tokens = await helper.getValidTokens([userRole]);
      accessToken = tokens.accessToken;
      myUserId = tokens.userId;
    });

    test('creating a user succeeds', async () => {
      const newUser = {
        username: 'alice@example.com',
        password: 'mySecurePassword',
      };
      await api
        .post('/api/user')
        .send(newUser)
        .expect(201)
        .expect('Content-Type', /application\/json/);
    });

    test('listing users fails', async () => {
      const response = await api
        .get('/api/user')
        .set('Authorization', `bearer ${accessToken}`)
        .expect(403)
        .expect('Content-Type', /application\/json/);
      expect(response.body.error).toMatch(/request denied/i);
    });

    test('querying other users by id succeeds', async () => {
      const userId = await helper.getValidUserId();
      expect(userId).not.toEqual(myUserId);

      await api
        .get(`/api/user/${userId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .expect(200)
        .expect('Content-Type', /application\/json/);
    });

    test('querying own user account by id succeeds', async () => {
      await api
        .get(`/api/user/${myUserId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .expect(200)
        .expect('Content-Type', /application\/json/);
    });

    test('editing another user account fails', async () => {
      const userId = await helper.getValidUserId();
      expect(userId).not.toEqual(myUserId);

      const editedUser = {
        username: 'ada@example.com',
        name: 'Ada Lovelace',
      };
      const response = await api
        .put(`/api/user/${userId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .send(editedUser)
        .expect(403)
        .expect('Content-Type', /application\/json/);
      expect(response.body.error).toMatch(/request denied/i);
    });

    test('editing own user account succeeds', async () => {
      const editedUser = {
        username: 'charlyb@example.com',
        name: 'Charles Babbage',
      };
      await api
        .put(`/api/user/${myUserId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .send(editedUser)
        .expect(200)
        .expect('Content-Type', /application\/json/);
    });

    test('editing roles of own user account fails', async () => {
      const editedUser = {
        username: 'charlyb@example.com',
        name: 'Charles Babbage',
        roles: [userRole, Role.Administrator],
      };
      await api
        .put(`/api/user/${myUserId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .send(editedUser)
        .expect(403)
        .expect('Content-Type', /application\/json/);
    });

    test('deleting another user account fails', async () => {
      const userId = await helper.getValidUserId();
      expect(userId).not.toEqual(myUserId);

      const response = await api
        .delete(`/api/user/${userId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .expect(403)
        .expect('Content-Type', /application\/json/);
      expect(response.body.error).toMatch(/request denied/i);
    });

    test('deleting own user account succeeds', async () => {
      await api
        .delete(`/api/user/${myUserId}`)
        .set('Authorization', `bearer ${accessToken}`)
        .expect(204);
    });
  },
);

describe('when not logged in', () => {
  test('listing users fails', async () => {
    const response = await api
      .get('/api/user')
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toContain('token missing');
  });

  test('querying users by id fails', async () => {
    const userId = await helper.getValidUserId();

    await api
      .get(`/api/user/${userId}`)
      .expect(401)
      .expect('Content-Type', /application\/json/);
  });

  describe('creating a user', () => {
    test('succeeds with username and password', async () => {
      const newUser = {
        username: 'alice@example.com',
        password: 'mySecurePassword',
      };
      const postResponse = await api
        .post('/api/user')
        .send(newUser)
        .expect(201)
        .expect('Content-Type', /application\/json/);
      expect(postResponse.body.username).toEqual(newUser.username);

      const usersAtEnd = await helper.usersInDb();
      expect(usersAtEnd.map((u) => u.username)).toContain(newUser.username);
    });

    test('has the "guest" role assigned', async () => {
      const newUser = {
        username: 'alice@example.com',
        password: 'mySecurePassword',
      };
      const response = await api
        .post('/api/user')
        .send(newUser)
        .expect(201)
        .expect('Content-Type', /application\/json/);
      expect(response.body.roles).toHaveLength(1);
      expect(response.body.roles).toContain(Role.Guest);
    });

    test('fails without a username', async () => {
      const newUser = { password: 'superSecure9000' };
      const response = await api
        .post('/api/user')
        .send(newUser)
        .expect(400)
        .expect('Content-Type', /application\/json/);
      expect(response.body.error).toMatch(/username is a required field/i);
    });

    test('fails if username is not a valid email address', async () => {
      const newUser = {
        username: 'alice',
        password: 'mySecurePassword',
      };
      const response = await api
        .post('/api/user')
        .send(newUser)
        .expect(400)
        .expect('Content-Type', /application\/json/);
      expect(response.body.error).toMatch(/username must be a valid email/i);
    });

    test('fails without a password', async () => {
      const newUser = { username: 'bobross' };
      const response = await api
        .post('/api/user')
        .send(newUser)
        .expect(400)
        .expect('Content-Type', /application\/json/);
      expect(response.body.error).toMatch(/password is a required field/i);
    });

    test('fails with a short password', async () => {
      const newUser = { username: 'alice', password: 'bob' };
      const response = await api
        .post('/api/user')
        .send(newUser)
        .expect(400)
        .expect('Content-Type', /application\/json/);
      expect(response.body.error).toMatch(
        /password must be at least \d+ characters/i,
      );
    });

    test('fails if username already exists', async () => {
      const newUser = {
        username: helper.initialUsers[0].username,
        password: 'superSecure9000',
      };
      const response = await api
        .post('/api/user')
        .send(newUser)
        .expect(400)
        .expect('Content-Type', /application\/json/);
      expect(response.body.error).toMatch(/username already exists/i);
    });

    test('fails when attempting to specify roles', async () => {
      const newUser = {
        username: 'alice@example.com',
        password: 'mySecurePassword',
        roles: [Role.Administrator, Role.Editor],
      };
      await api
        .post('/api/user')
        .send(newUser)
        .expect(401)
        .expect('Content-Type', /application\/json/);
    });

    test('sets the email address to unverified by default', async () => {
      const newUser = {
        username: 'ada@example.com',
        password: 'babbageAnalytical',
      };
      const response = await api
        .post('/api/user')
        .send(newUser)
        .expect(201)
        .expect('Content-Type', /application\/json/);

      const userInDb = await User.findById(response.body.id);
      if (!userInDb) throw Error('user not found');
      expect(userInDb.emailVerified).toBeFalsy();
    });
  });

  test('deleting a user fails', async () => {
    const userId = await helper.getValidUserId();
    const response = await api
      .delete(`/api/user/${userId}`)
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(/token missing/i);
  });

  test('editing a user fails', async () => {
    const userId = await helper.getValidUserId();
    const editedUser = {
      username: 'alice@example.com',
      name: 'Alice Reed',
    };
    const response = await api
      .put(`/api/user/${userId}`)
      .send(editedUser)
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(/token missing/i);

    const usersAtEnd = await helper.usersInDb();
    expect(usersAtEnd.map((u) => u.username)).not.toContain(
      'alice@example.com',
    );
  });
});

describe('verifying an email address', () => {
  const testUser = {
    username: 'ada@example.org',
    password: 'theAnalyticalEngineChooChoo',
  };

  beforeEach(async () => {
    await api.post('/api/user').send(testUser).expect(201);
  });

  test('succeeds with a valid verification code', async () => {
    const emailVerificationCode = await helper.getEmailVerificationCode(
      testUser.username,
    );

    const response = await api
      .post(`/api/user/verify_email/${emailVerificationCode}`)
      .expect(200)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toBeUndefined();
    expect(response.body.message).toMatch(/email address verified/i);

    const userInDb = await User.findOne({ username: testUser.username });
    if (!userInDb) throw Error('user not found');
    expect(userInDb.emailVerified).toBeTruthy();
    expect(userInDb.emailVerificationCode).toEqual(emailVerificationCode);
  });

  test('fails with a verification code that has the right length but is invalid', async () => {
    const response = await api
      .post(
        '/api/user/verify_email/qainws1a6pd8nt4pdi6yur9xc6yhxb5ddi92o35r1109kqki',
      )
      .expect(404)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(
      /user not found or verification code invalid/i,
    );
    expect(response.body.message).toBeUndefined();
  });

  test.each(['', ' ', 'a', 'qwertyui'])(
    "fails with verification code '%s' that is technically valid but too short",
    async (code) => {
      const user = new User({
        username: 'whitney@example.com',
        passwordHash: hashPassword('ooohIwannaDanceWithSomebody'),
        emailVerified: false,
        emailVerificationCode: code,
      });
      await user.save();

      const response = await api.post('/api/user/verify_email/a').expect(400);
      expect(response.body.error).toMatch(/verification code invalid/i);
      expect(response.body.message).toBeUndefined();
    },
  );
});

describe('forgot password', () => {
  test('fails when no email is specified', async () => {
    await api.post(`/api/user/forgot_password`).expect(400);
  });

  test('does not leak information about which users exist', async () => {
    // this user does not exist
    const badRequest = await api
      .post(`/api/user/forgot_password`)
      .send({ email: 'thisUserIsNotInTheDatabase@example.com' });

    // this user exists
    const user = await User.findOne({});
    if (!user) throw 'User database is empty';
    const goodRequest = await api
      .post(`/api/user/forgot_password`)
      .send({ email: user.username });

    // assertions
    expect(badRequest.status).toEqual(goodRequest.status);
    expect(badRequest.body).toEqual(goodRequest.body);
  });

  test('sets a forgetPasswordCode when a valid email address is given', async () => {
    const userBeforeCall = await User.findOne({});
    if (!userBeforeCall) throw 'User database is empty';

    expect(userBeforeCall.resetPasswordCode).toBeFalsy();

    await api
      .post(`/api/user/forgot_password`)
      .send({ email: userBeforeCall.username })
      .expect(200);

    const userAfterCall = await User.findById(userBeforeCall.id);
    if (!userAfterCall) throw 'User no longer in database';

    expect(userAfterCall.resetPasswordCode).toHaveLength(48);
  });
});

describe('reset password', () => {
  test('succeeds with a valid reset code and email', async () => {
    const user = new User({
      username: 'grace@example.org',
      passwordHash: hashPassword('superSecurePw'),
      resetPasswordCode: 'ovCMl52Kme1O1zgqMl1ykw8nEfAvmhOCmju2hMX8GGlJa8Jl',
    });
    await user.save();

    const response = await api
      .post('/api/user/reset_password')
      .send({
        code: user.resetPasswordCode,
        password: 'evenSecurerPw',
      })
      .expect(200)
      .expect('Content-Type', /application\/json/);

    expect(response.body.message).toMatch(/password changed/i);
  });

  test('fails with an invalid reset code', async () => {
    const response = await api
      .post('/api/user/reset_password')
      .send({
        code: 'ovCMl52Kme1O1zgqMl1ykw8nEfAvmhOCmju2hMX8GGlJa8Jl',
        password: 'aNewSecurePassword',
      })
      .expect(400)
      .expect('Content-Type', /application\/json/);

    expect(response.body.error).toMatch(/invalid email address or reset code/i);
  });

  test("fails with a valid reset code but a password that's too short", async () => {
    const user = new User({
      username: 'grace@example.org',
      passwordHash: hashPassword('superSecurePw'),
      resetPasswordCode: 'ovCMl52Kme1O1zgqMl1ykw8nEfAvmhOCmju2hMX8GGlJa8Jl',
    });
    await user.save();

    const response = await api
      .post('/api/user/reset_password')
      .send({
        code: user.resetPasswordCode,
        password: 'short',
      })
      .expect(400)
      .expect('Content-Type', /application\/json/);

    expect(response.body.error).toMatch(
      /password must be at least 12 characters/i,
    );
  });
});

afterAll(() => {
  void mongoose.connection.close();
});
