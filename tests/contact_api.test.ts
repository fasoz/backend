import supertest from 'supertest';
import app from '../src/app';

const api = supertest(app);

describe('submitting a message', () => {
  test('succeeds with just an email and a message', async () => {
    const payload = {
      email: 'charles@example.org',
      message: 'lets get analytical',
    };

    const response = await api
      .post('/api/contact')
      .send(payload)
      .expect(200)
      .expect('Content-Type', /application\/json/);

    expect(response.body.sender).toEqual(payload.email);
    expect(response.body.subject).toEqual('Ohne Betreff');
    expect(response.body.message).toContain(payload.message);
  });

  test('succeeds with all fields', async () => {
    const payload = {
      name: 'Charles Babbage',
      email: 'charles@example.org',
      subject: 'Proposition',
      message: 'lets get analytical',
    };

    const response = await api
      .post('/api/contact')
      .send(payload)
      .expect(200)
      .expect('Content-Type', /application\/json/);

    expect(response.body.sender).toEqual(`${payload.name} <${payload.email}>`);
    expect(response.body.subject).toEqual(payload.subject);
    expect(response.body.message).toContain(payload.message);
  });

  test('fails with an email missing', async () => {
    await api
      .post('/api/contact')
      .send({ message: 'Hello there' })
      .expect(400)
      .expect('Content-Type', /application\/json/);
  });

  test('fails with the message missing', async () => {
    await api
      .post('/api/contact')
      .send({ email: 'charles@example.org' })
      .expect(400)
      .expect('Content-Type', /application\/json/);
  });
});
