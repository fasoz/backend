import bcrypt from 'bcryptjs';
import config from 'config';
import supertest from 'supertest';
import app from '../src/app';
import Case from '../src/models/case';
import User from '../src/models/user';
import minioClient from '../src/utils/minioClient';
import * as types from '../src/types';
import jwt from 'jsonwebtoken';
import { Role } from '../src/utils/rbac';
import caseSearch from '../src/services/caseSearch';

const api = supertest(app);

const casesInDb = async (): Promise<Array<types.CaseDocument>> => {
  return Case.find({});
};

const usersInDb = async (): Promise<Array<types.UserDocument>> => {
  return User.find({});
};

const deleteAllUsers = (): Promise<void> => {
  return new Promise((resolve, reject) => {
    User.deleteMany({})
      .then(() => resolve())
      .catch((error) => reject(error));
  });
};

const getValidCaseId = async (): Promise<string> => {
  return new Promise((resolve, reject) => {
    Case.findOne({ published: true })
      .then((thisCase) => {
        if (!thisCase?._id) reject();
        else resolve(thisCase._id);
      })
      .catch((error) => reject(error));
  });
};

const getValidUserId = async (): Promise<string> => {
  return new Promise((resolve, reject) => {
    User.findOne({})
      .then((user) => {
        if (!user?._id) reject();
        else resolve(user._id);
      })
      .catch((error) => reject(error));
  });
};

const initialCases = [
  {
    title: 'Jacky',
    description:
      'orem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.',
    fileCategories: ['Beep', 'Boop'],
    keywords: ['lorem', 'ipsum', 'dolor', 'amet'],
    participants: 'Mom, Dad, Sister, Brother',
    pedagogicFields: ['Cetero Appetere'],
    problemAreas: ['Nostrud Facilisis', 'Civibus Complectitur '],
    published: true,
    fileAccessAllowedRoles: [
      Role.Administrator,
      Role.Editor,
      Role.Researcher,
      Role.Guest,
    ],
  },
  {
    title: 'Kevin',
    description:
      ' Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
    fileCategories: ['La', 'Le', 'Lu'],
    keywords: ['ipsum', 'dolor', 'sit'],
    participants: 'Rathalos, Harry Potter',
    pedagogicFields: ['Scriptorem'],
    problemAreas: ['Aperiam Persius', 'Nostrud Facilisis'],
    published: true,
    fileAccessAllowedRoles: [Role.Administrator, Role.Editor, Role.Researcher],
  },
  {
    title: 'Incomplete draft',
    description: '',
    fileCategories: [],
    keywords: [],
    participants: '',
    pedagogicFields: [],
    problemAreas: [],
    published: false,
    fileAccessAllowedRoles: [
      Role.Administrator,
      Role.Editor,
      Role.Researcher,
      Role.Guest,
    ],
  },
];
// populates the database with hardcoded cases for testing purposes
const prepareCaseFixtures = async (): Promise<void> => {
  await Case.deleteMany({});

  const mongoPromises = initialCases.map(async (thisCase) => {
    const userObject = new Case(thisCase);
    return userObject.save();
  });

  const savedCases = await Promise.all(mongoPromises);

  const elasticsearchPromises = savedCases.map((thisCase) => {
    return caseSearch.addCaseToIndex(thisCase);
  });

  await Promise.all(elasticsearchPromises);
};

const initialUsers = [
  {
    name: 'Ian Root',
    username: 'admin@example.com',
    password: 'admin123456789',
    roles: [Role.Administrator],
  },
  {
    name: 'Kimi Räikkönen',
    username: 'kimi@example.com',
    password: 'iKnowWhatImDoing',
    roles: [Role.Editor],
  },
  {
    name: 'Juka Iracinen',
    username: 'juka@example.org',
    password: 'ScandinavianFlick',
    roles: [Role.Researcher],
  },
  {
    name: 'John Deere',
    username: 'johnny@example.org',
    password: 'ohDeereOhDeere',
    roles: [],
  },
];

// populates the database with hardcoded users for testing purposes
const prepareUserFixtures = async (): Promise<void> => {
  await User.deleteMany({});

  const promiseArray = initialUsers.map(async (user) => {
    const newUser = {
      emailVerified: true,
      name: user.name,
      username: user.username,
      passwordHash: bcrypt.hashSync(user.password, 10),
      roles: user.roles,
    };
    const userObject = new User(newUser);
    return userObject.save();
  });
  await Promise.all(promiseArray);
};

const getEmailVerificationCode = async (email: string): Promise<string> => {
  const user = await User.findOne({ username: email });
  if (!user) throw Error('User not found');
  return user.emailVerificationCode;
};

/**
 * Returns an access and a refresh token
 * @param roles - specify if you need tokens with specific roles
 */
const getValidTokens = async (
  roles: Array<Role> = [],
): Promise<{ accessToken: string; refreshToken: string; userId: string }> => {
  const user = initialUsers[1];

  // set roles for this user
  await User.findOneAndUpdate({ username: user.username }, { $set: { roles } });

  const response = await api
    .post('/api/auth/login')
    .send({
      username: user.username,
      password: user.password,
    })
    .expect(200)
    .expect('Content-Type', /application\/json/);

  const regex = new RegExp(
    /^refresh-token=(.+); Path=\/api\/auth\/; Expires=.*$/,
  );

  const cookies = response.header['set-cookie'] as Array<string>;

  const cookieHeader = cookies.find((c: string) => regex.exec(c));
  if (!cookieHeader) throw new Error('set-cookie header has no refresh token');

  const matches = regex.exec(cookieHeader);
  if (!matches) throw new Error('set-cookie header has no refresh token');

  return {
    accessToken: response.body.token as string,
    refreshToken: matches[1],
    userId: response.body.id as string,
  };
};

const getExpiredToken = (): string => {
  const userForToken = {
    username: 'alice@example.org',
    id: 'qwertyuiop',
  };
  const jwtSecret: string = config.get('jwt.secret');
  return jwt.sign(userForToken, jwtSecret, {
    expiresIn: '-1h',
  });
};

// S3 storage helpers
const caseFilesBucket: string = config.get('minio.caseFilesBucket');

const getValidCaseFile = async (): Promise<{
  caseId: string;
  path: string;
}> => {
  const caseId = await getValidCaseId();
  if (!caseId) throw 'unable to get valid case id';

  await minioClient.fPutObject(
    caseFilesBucket,
    `${caseId}/example.pdf`,
    `${process.cwd()}/tests/fixtures/example.pdf`,
    {},
  );

  return { caseId, path: 'example.pdf' };
};

export default {
  casesInDb,
  deleteAllUsers,
  getEmailVerificationCode,
  getExpiredToken,
  getValidCaseFile,
  getValidCaseId,
  getValidTokens,
  getValidUserId,
  initialCases,
  initialUsers,
  prepareCaseFixtures,
  prepareUserFixtures,
  usersInDb,
};
