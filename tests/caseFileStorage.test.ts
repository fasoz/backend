import fs from 'fs';
import caseFileStorage from '../src/services/caseFileStorage';

const dummyCaseId = 'qwertyuiop3456789';

beforeEach(async () => {
  await caseFileStorage.deleteAllCases();
});

test('uploading a file', async () => {
  const fileStream = fs.createReadStream(
    `${process.cwd()}/tests/fixtures/example.pdf`,
  );
  const response = await caseFileStorage.storeFile(
    dummyCaseId,
    'subfolder42/example.pdf',
    fileStream,
  );

  expect(response.etag).toMatch(/^[0-9a-zA-Z]+$/);
  expect(response.versionId).toBeNull();
});

test('fetching a file', async () => {
  const fileStream = fs.createReadStream(
    `${process.cwd()}/tests/fixtures/example.pdf`,
  );
  await caseFileStorage.storeFile(
    dummyCaseId,
    'subfolder42/example.pdf',
    fileStream,
  );

  await caseFileStorage.fetchFile(dummyCaseId, 'subfolder42/example.pdf');
});

test('listing files', async () => {
  const fileStream = fs.createReadStream(
    `${process.cwd()}/tests/fixtures/example.pdf`,
  );
  await caseFileStorage.storeFile(
    dummyCaseId,
    'subfolder42/example.pdf',
    fileStream,
  );

  const files = await caseFileStorage.listFiles(dummyCaseId);
  expect(files.map((file) => file.path)).toContain('subfolder42/example.pdf');
});

test('deleting a file', async () => {
  const fileStream = fs.createReadStream(
    `${process.cwd()}/tests/fixtures/example.pdf`,
  );
  await caseFileStorage.storeFile(
    dummyCaseId,
    'subfolder42/example.pdf',
    fileStream,
  );
  await caseFileStorage.deleteFile(dummyCaseId, 'subfolder42/example.pdf');
  const files = await caseFileStorage.listFiles(dummyCaseId);
  expect(files.map((file) => file.path)).not.toContain(
    'subfolder42/example.pdf',
  );
});

test('bulk delete files', async () => {
  const paths = [
    `${process.cwd()}/tests/fixtures/example.pdf`,
    `${process.cwd()}/tests/fixtures/emptyFile.txt`,
  ];
  const storePromises = paths.map(async (path) => {
    const fileStream = fs.createReadStream(path);
    const fileName = path.split('/').reverse()[0];
    return caseFileStorage.storeFile(dummyCaseId, fileName, fileStream);
  });
  await Promise.all(storePromises);

  await caseFileStorage.deleteCase(dummyCaseId);
  const files = await caseFileStorage.listFiles(dummyCaseId);
  expect(files.length).toEqual(0);
});
