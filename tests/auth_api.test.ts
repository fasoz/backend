import jwt from 'jsonwebtoken';
import mongoose from 'mongoose';
import supertest from 'supertest';
import app from '../src/app';
import helper from './test_helper';
import User from '../src/models/user';
import { Role } from '../src/utils/rbac';
import config from 'config';

const api = supertest(app);

const jwtSecret: string = config.get('jwt.secret');

/**
 * Finds the refresh-token in a http 'set-cookie' header and returns it as a string
 * @param cookies - array of strings from the set-cookie http header
 * @returns string - refresh-token
 */
const extractRefreshTokenFromCookies = (cookies: Array<string>): string => {
  const cookie = cookies.find((c) => c.startsWith('refresh-token='));
  if (!cookie) throw 'refresh-token not found in cookies';
  return cookie.substr(14).split(';')[0];
};

/**
 * Determines the expiration date of the refresh token cookie
 * @param cookies - Array of cookie strings
 * @returns Date object holding the expiration date
 */
const refreshTokenExpiration = (cookies: Array<string>) => {
  const refreshTokenCookie = cookies.find(
    (c) => c.substr(0, 14) === 'refresh-token=',
  );
  if (!refreshTokenCookie) throw new Error('refresh-token not found');

  const regex = /Expires=(\w{3}, \d{2} \w{3} \d{4} \d{2}:\d{2}:\d{2} \w{3})/;
  const match = regex.exec(refreshTokenCookie);
  if (!match) throw new Error('unable to determine cookie expiration');
  return Date.parse(match[1]);
};

beforeEach(async () => {
  await helper.prepareUserFixtures();
});

test('login succeeds with valid credentials', async () => {
  const loginData = {
    ...helper.initialUsers[0],
  };
  const response = await api
    .post('/api/auth/login')
    .send(loginData)
    .expect(200)
    .expect('Content-Type', /application\/json/)
    .expect('set-cookie', /refresh-token=.+; Path=\//);
  expect(response.body.token).toBeDefined();
  expect(response.body.token).toBeTruthy();
});

test('login fails with invalid credentials', async () => {
  const loginData = {
    username: 'bob@example.com',
    password: 'ididnotsignupforthis',
  };
  await api
    .post('/api/auth/login')
    .send(loginData)
    .expect(401)
    .expect('Content-Type', /application\/json/);
});

test('login fails with unverified email address', async () => {
  const loginData = {
    username: 'ada@example.org',
    password: 'babbageAnalytical',
  };

  await api.post('/api/user').send(loginData).expect(201);

  const response = await api
    .post('/api/auth/login')
    .send(loginData)
    .expect(401)
    .expect('Content-Type', /application\/json/);

  expect(response.body.error).toMatch(/email verification pending/i);
});

test('login fails without username', async () => {
  const loginData = {
    password: 'loremipsum',
  };
  await api
    .post('/api/auth/login')
    .send(loginData)
    .expect(400)
    .expect('Content-Type', /application\/json/);
});

test('login fails without password', async () => {
  const loginData = {
    username: 'alice',
  };
  await api
    .post('/api/auth/login')
    .send(loginData)
    .expect(400)
    .expect('Content-Type', /application\/json/);
});

test('login fails with empty password', async () => {
  const loginData = {
    username: 'alice',
    password: '',
  };
  await api
    .post('/api/auth/login')
    .send(loginData)
    .expect(400)
    .expect('Content-Type', /application\/json/);
});

test('username is encoded in the access token', async () => {
  const loginData = {
    ...helper.initialUsers[0],
  };
  const response = await api
    .post('/api/auth/login')
    .send(loginData)
    .expect(200)
    .expect('Content-Type', /application\/json/);

  const jwtPayload = jwt.decode(response.body.token, { json: true });
  expect(jwtPayload).not.toBeNull();
  if (jwtPayload != null)
    expect(jwtPayload.username).toEqual(loginData.username);
});

test('access token has a valid signature', async () => {
  const loginData = {
    ...helper.initialUsers[0],
  };
  const response = await api
    .post('/api/auth/login')
    .send(loginData)
    .expect(200)
    .expect('Content-Type', /application\/json/);

  expect(jwt.verify(response.body.token, jwtSecret)).toBeTruthy();
});

test('access token is valid for no more than 15 minutes', async () => {
  const loginData = {
    ...helper.initialUsers[0],
  };
  const response = await api
    .post('/api/auth/login')
    .send(loginData)
    .expect(200)
    .expect('Content-Type', /application\/json/);

  const jwtPayload = jwt.decode(response.body.token, { json: true });
  expect(jwtPayload?.exp).toBeDefined();
  expect(jwtPayload?.iat).toBeDefined();
  if (jwtPayload?.exp && jwtPayload?.iat) {
    expect(jwtPayload.exp - jwtPayload.iat).toBeLessThanOrEqual(15 * 60 * 60);
  }
});

test('logout unsets the refresh cookie', async () => {
  const loginData = {
    ...helper.initialUsers[0],
  };
  const loginResponse = await api
    .post('/api/auth/login')
    .send(loginData)
    .expect(200);
  const cookies = loginResponse.header['set-cookie'];

  const logoutResponse = await api
    .get('/api/auth/logout')
    .set('Cookie', cookies)
    .expect(200)
    .expect('Content-Type', /application\/json/);

  const expirationDate = refreshTokenExpiration(
    logoutResponse.header['set-cookie'],
  );
  expect(expirationDate).toBeLessThanOrEqual(Date.now());
});

test('logout revokes the refresh token', async () => {
  const loginData = {
    ...helper.initialUsers[0],
  };
  const loginResponse = await api
    .post('/api/auth/login')
    .send(loginData)
    .expect(200);
  const cookies = loginResponse.header['set-cookie'];

  await api
    .get('/api/auth/logout')
    .set('Cookie', cookies)
    .expect(200)
    .expect('Content-Type', /application\/json/);

  const userInDb = await User.findById(loginResponse.body.id);
  expect(userInDb?.sessions).toHaveLength(0);
});

test('logout without login does nothing', async () => {
  const response = await api
    .get('/api/auth/logout')
    .expect(200)
    .expect('Content-Type', /application\/json/);
  expect(response.body.message).toMatch(/already logged out/i);
});

test('refresh token has a valid signature', async () => {
  const loginData = {
    ...helper.initialUsers[0],
  };
  const response = await api
    .post('/api/auth/login')
    .send(loginData)
    .expect(200)
    .expect('Content-Type', /application\/json/)
    .expect('set-cookie', /refresh-token=.+;/);

  const refreshToken = extractRefreshTokenFromCookies(
    response.header['set-cookie'],
  );
  expect(jwt.verify(refreshToken, jwtSecret)).toBeTruthy();
});

test('refresh token has hardened cookie options', async () => {
  const loginData = {
    ...helper.initialUsers[0],
  };
  const response = await api
    .post('/api/auth/login')
    .send(loginData)
    .expect(200)
    .expect('Content-Type', /application\/json/)
    .expect('set-cookie', /refresh-token=.+;/);
  expect(response.header['set-cookie'][0]).toMatch(/Path=\/api\//);
  expect(response.header['set-cookie'][0]).toMatch(/Expires=.+/);
  expect(response.header['set-cookie'][0]).toMatch(/HttpOnly/);
  expect(response.header['set-cookie'][0]).toMatch(/SameSite=Strict/);
});

test('refresh token has a reasonable expiration date', async () => {
  const loginData = {
    ...helper.initialUsers[0],
  };
  const response = await api
    .post('/api/auth/login')
    .send(loginData)
    .expect(200)
    .expect('Content-Type', /application\/json/)
    .expect('set-cookie', /refresh-token=.+;/);
  const expirationDate = refreshTokenExpiration(response.header['set-cookie']);
  expect(expirationDate).toBeGreaterThan(Date.now());
  expect(expirationDate).toBeLessThan(Date.now() + 366 * 24 * 3600000); // just over a year
});

describe('accessing a protected endpoint', () => {
  test('without a token returns an error', async () => {
    const response = await api
      .get('/api/user')
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(/token missing/i);
  });

  test('with an expired token returns an error', async () => {
    const expiredToken = helper.getExpiredToken();
    const response = await api
      .get('/api/user')
      .set('Authorization', `bearer ${expiredToken}`)
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(/token expired/i);
  });

  test('with a token that has been refreshed succeeds', async () => {
    const refreshTokens = (await helper.getValidTokens([Role.Administrator]))
      .refreshToken;
    const response = await api
      .get('/api/auth/verify_session')
      .set('Cookie', [`refresh-token=${refreshTokens}`]);
    const accessToken = response.body.token;
    await api
      .get('/api/user')
      .set('Authorization', `bearer ${accessToken}`)
      .expect(200);
  });
});

describe('verifying a session / obtaining new tokens', () => {
  let refreshToken: string;
  let myUserId: string;

  beforeEach(async () => {
    const tokens = await helper.getValidTokens();
    refreshToken = tokens.refreshToken;
    myUserId = tokens.userId;
  });

  test('with a valid refresh token succeeds', async () => {
    const response = await api
      .get('/api/auth/verify_session')
      .set('Cookie', [`refresh-token=${refreshToken}`])
      .expect(200)
      .expect('Content-Type', /application\/json/);
    expect(response.body.token).toBeTruthy();
    expect(response.body.username).toBeTruthy();
  });

  test('with a valid refresh token returns a valid access token', async () => {
    const response = await api
      .get('/api/auth/verify_session')
      .set('Cookie', [`refresh-token=${refreshToken}`])
      .expect(200)
      .expect('Content-Type', /application\/json/);

    // user id of old and new access token match
    expect(response.body.id).toBe(myUserId);

    // new access token has a valid signature
    expect(jwt.verify(response.body.token, jwtSecret)).toBeTruthy();
  });

  test('with a valid refresh token rotates the refresh token', async () => {
    // wait for one second so that the new refresh token differs from the old one
    await new Promise((resolve) => setTimeout(resolve, 1000));

    const response = await api
      .get('/api/auth/verify_session')
      .set('Cookie', [`refresh-token=${refreshToken}`])
      .expect(200)
      .expect('Content-Type', /application\/json/)
      .expect('set-cookie', /refresh-token=.+;/);

    // only one cookie expected, so the first cookie should be the refresh-token
    expect(
      response.header['set-cookie'][0].startsWith('refresh-token='),
    ).toBeTruthy();
    const newRefreshToken = extractRefreshTokenFromCookies(
      response.header['set-cookie'],
    );

    // the newly set refresh token is different to the one that was used in the request
    expect(newRefreshToken).not.toEqual(refreshToken);

    // new refresh token has a valid signature
    expect(jwt.verify(newRefreshToken, jwtSecret)).toBeTruthy();
  });

  test('without a refresh token returns nothing (204)', async () => {
    await api.get('/api/auth/verify_session').expect(204);
  });

  test('with an invalid refresh token fails', async () => {
    const response = await api
      .get('/api/auth/verify_session')
      .set('Cookie', ['refresh-token=1234567890qwertyuiop'])
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(/invalid refresh token/i);
  });

  test('with a valid refresh token for a user that no longer exists fails', async () => {
    await helper.deleteAllUsers();
    const response = await api
      .get('/api/auth/verify_session')
      .set('Cookie', [`refresh-token=${refreshToken}`])
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(response.body.error).toMatch(/invalid user/i);
  });

  test('with a revoked refresh token fails', async () => {
    const tokens = await helper.getValidTokens([Role.Editor]);
    const updateResponse = await api
      .put(`/api/user/${myUserId}`)
      .set('Authorization', `bearer ${tokens.accessToken}`)
      .send({
        username: 'ada.lovelace@example.org',
        name: 'Ada Lovelace',
        sessions: [],
      })
      .expect(200);
    expect(updateResponse.body.sessions.length).toBe(0);

    const refreshResponse = await api
      .get('/api/auth/verify_session')
      .set('Cookie', [`refresh-token=${tokens.refreshToken}`])
      .expect(401)
      .expect('Content-Type', /application\/json/);
    expect(refreshResponse.body.error).toMatch(/session revoked/i);
  });
});

afterAll(() => {
  void mongoose.connection.close();
});
